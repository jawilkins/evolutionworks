package evolutionworks.model;

/**
 * @author Jason Wilkins
 *
 */
public interface ModelPaper
{
    String getLocation();
    int getCitationCount();
    int getReferenceCount();
    String getTitle();
    int getAuthorCount();
    String getAuthor(int index);
    String getAbstract();
    ModelPaperList getCitations();
    ModelPaperList getReferences();
    java.util.Collection<String> getLinks();
    void fullyPopulate();
}
