package evolutionworks.model;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author Jason Wilkins
 *
 */
public class ModelPaperList implements Iterable<ModelPaper>
{
    private Collection<ModelPaper> papers;

    protected ModelPaperList(Collection<ModelPaper> papers)
    {
        this.setPapers(papers);
    }

    public final int size()
    {
        return getPapers().size();
    }

    public final Iterator<ModelPaper> iterator()
    {
        return getPapers().iterator();
    }

    protected Collection<ModelPaper> getPapers()
    {
        return papers;
    }

    protected final void setPapers(Collection<ModelPaper> papers)
    {
        this.papers = papers;
    }
}
