package evolutionworks.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class ModelPaperLinkedList extends ModelPaperList
{
    private final Map<String, ModelPaper> paperMap;

    public ModelPaperLinkedList()
    {
        super(null);
        paperMap = new LinkedHashMap<String, ModelPaper>();
        setPapers(paperMap.values());
    }

    public final boolean contains(ModelPaper modelPaper)
    {
        return getPapers().contains(modelPaper);
    }

    public final void add(ModelPaper modelPaper)
    {
        paperMap.put(modelPaper.getLocation(), modelPaper);
    }

    public final void remove(String location)
    {
        paperMap.remove(location);
    }

    public final void remove(ModelPaper modelPaper)
    {
        getPapers().remove(modelPaper);
    }
}
