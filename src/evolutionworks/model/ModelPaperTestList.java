package evolutionworks.model;

import java.util.ArrayList;
import java.util.List;

import evolutionworks.model.ModelPaperTest.Corpus;

/**
 * @author Jason Wilkins
 *
 */
class ModelPaperTestList extends ModelPaperList
{
    ModelPaperTestList(int indexes[], Corpus corpus)
    {
        super(init(indexes, corpus));
    }

    private static List<ModelPaper> init(int[] indexes, Corpus corpus)
    {
        List<ModelPaper> papers = new ArrayList<ModelPaper>(indexes.length);

        for (int i = 0; i < indexes.length; i++) {
            papers.add(new ModelPaperTest(indexes[i], corpus));
        }

        return papers;
    }
}
