package evolutionworks.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Random;

import org.apache.commons.lang3.text.WordUtils;

import de.svenjacobs.loremipsum.LoremIpsum;

/**
 * @author Jason Wilkins
 *
 */
public class ModelPaperTest implements ModelPaper
{
    private int                 index;

    private Corpus              corpus;
    private int[]               citationIndexes;
    private int[]               referenceIndexes;

    private String              title;

    private String[]            authors;

    private String              strAbstract;

    private ModelPaperTestList  citations;
    private ModelPaperTestList  references;

    private static final int    MAX_LINKS           = 100;
    private static final double LINK_EXPONENT       = 3;
    private static final double LINK_COUNT_EXPONENT = 2;
    private static final int    MAX_AUTHORS         = 6;
    private static final double AUTHOR_EXPONENT     = 3;
    private static final int    LOREM_WORD_MAX      = 50;
    private static final int    TITLE_WORD_MAX      = 10;
    private static final int    TITLE_WORD_MIN      = 2;
    private static final int    TITLE_WORD_START    = LOREM_WORD_MAX - TITLE_WORD_MAX - TITLE_WORD_MIN;
    private static final int    AUTHOR_WORD_MAX     = 3;
    private static final int    AUTHOR_WORD_MIN     = 2;
    private static final int    AUTHOR_WORD_START   = LOREM_WORD_MAX - AUTHOR_WORD_MAX - AUTHOR_WORD_MIN;

    private String cleanup(String original)
    {
        return original.replaceAll("\\.", "").replaceAll(",", "");
    }

    static class Corpus
    {
        private final long[] corpus;

        public Corpus(int corpusLength, long seed)
        {
            final Random random = new Random(seed);
            corpus = new long[corpusLength];

            for (int i = 0; i < corpusLength; i++) {
                corpus[i] = random.nextLong();
            }
        }

        /**
         * @return the corpus
         */
        long get(int index)
        {
            return corpus[index];
        }

        int size()
        {
            return corpus.length;
        }

    }

    public ModelPaperTest(int index, Corpus corpus)
    {
        this.corpus = corpus;

        this.index = index;

        long seed = corpus.get(index);

        final LoremIpsum loremIpsum = new LoremIpsum();

        final Random random = new Random(seed);

        title =
            WordUtils.capitalizeFully(cleanup(loremIpsum.getWords(
                random.nextInt(TITLE_WORD_MAX - TITLE_WORD_MIN) + TITLE_WORD_MIN,
                random.nextInt(TITLE_WORD_START))));

        genLinks(seed, random);

        genAuthors(loremIpsum, random);

        strAbstract = loremIpsum.getParagraphs(random.nextInt(2) + 1);
    }

    private void genLinks(long seed, final Random random)
    {
        final int linkCount = (int) (Math.pow(random.nextDouble(), LINK_COUNT_EXPONENT) * MAX_LINKS);
        final ArrayList<Integer> links = new ArrayList<Integer>(linkCount);

        int referenceCount = 0;
        int citationCount = 0;
        for (int i = 0; i < linkCount; i++) {
            final int newIndex = (int) (Math.pow(random.nextDouble(), LINK_EXPONENT) * corpus.size());

            if (newIndex < corpus.size() && !links.contains(newIndex)) {
                final long newSeed = corpus.get(newIndex);

                if (newSeed < seed) {
                    referenceCount++;
                }
                else if (newSeed > seed) {
                    citationCount++;
                }
                else {
                    break; // should not cite ourselves
                }
            }
            else {
                break;
            }

            links.add(newIndex);
        }

        citationIndexes = new int[citationCount];
        referenceIndexes = new int[referenceCount];
        {
            int i = 0;
            int j = 0;
            for (int linkIndex : links) {
                if (corpus.get(linkIndex) < seed) {
                    referenceIndexes[i++] = linkIndex;
                }
                else {
                    citationIndexes[j++] = linkIndex;
                }
            }
        }
    }

    private void genAuthors(final LoremIpsum loremIpsum, final Random random)
    {
        final int authorCount = (int) (Math.pow(random.nextDouble(), AUTHOR_EXPONENT) * MAX_AUTHORS) + 1;
        authors = new String[authorCount];

        for (int i = 0; i < authorCount; i++) {
            authors[i] =
                cleanup(loremIpsum.getWords(
                    random.nextInt(AUTHOR_WORD_MAX - AUTHOR_WORD_MIN) + AUTHOR_WORD_MIN,
                    random.nextInt(AUTHOR_WORD_START)));

            authors[i] = WordUtils.capitalizeFully(authors[i]);
        }
    }

    @Override
    public String getLocation()
    {
        return "test:" + index;
    }

    @Override
    public int getCitationCount()
    {
        return citationIndexes.length;
    }

    @Override
    public int getReferenceCount()
    {
        return referenceIndexes.length;
    }

    @Override
    public String getTitle()
    {
        return title;
    }

    @Override
    public int getAuthorCount()
    {
        return authors.length;
    }

    @Override
    public String getAuthor(int index)
    {
        return authors[index];
    }

    @Override
    public String getAbstract()
    {
        return strAbstract;
    }

    @Override
    public ModelPaperTestList getCitations()
    {
        initCitations();

        return citations;
    }

    public void initCitations()
    {
        if (citations == null) {
            citations = new ModelPaperTestList(citationIndexes, corpus);
        }
    }

    @Override
    public ModelPaperTestList getReferences()
    {
        initReferences();

        return references;
    }

    public void initReferences()
    {
        if (references == null) {
            references = new ModelPaperTestList(referenceIndexes, corpus);
        }
    }

    @Override
    public Collection<String> getLinks()
    {
        Collection<String> links = new LinkedList<String>();

        initReferences();
        initCitations();
        
        for (ModelPaper link : citations) {
            links.add(link.getLocation());
        }
        
        for (ModelPaper link : references) {
            links.add(link.getLocation());
        }

        return links;
    }

    @Override
    public void fullyPopulate()
    {
    }
}
