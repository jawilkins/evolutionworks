package evolutionworks.model;

public interface ModelArrowRenderer
{
    void render(double x0, double y0, double x1, double y1, double width);
}
