package evolutionworks.model;

import java.util.ArrayList;
import java.util.List;

import ecologylab.semantics.generated.library.creativeWork.Author;
import ecologylab.semantics.generated.library.scholarlyPublication.ScholarlyArticle;

/**
 * @author Jason Wilkins
 *
 */
class ModelPaperScholarlyArticleList extends ModelPaperList
{
    public ModelPaperScholarlyArticleList(List<ScholarlyArticle> scholarlyArticles)
    {
        super(init(scholarlyArticles));
    }

    private static List<ModelPaper> init(List<ScholarlyArticle> scholarlyArticles)
    {
        List<ModelPaper> papers = new ArrayList<ModelPaper>(scholarlyArticles.size());

        for (ScholarlyArticle article : scholarlyArticles) {
            List<Author> authors = article.getAuthors();

            List<String> authorNames;

            if (authors != null) {
                authorNames = new ArrayList<String>(authors.size());

                for (Author author : article.getAuthors()) {
                    authorNames.add(author.getTitle());
                }
            }
            else {
                authorNames = new ArrayList<String>(0);
            }

            List<ScholarlyArticle> references = article.getReferences();
            int referenceCount = references == null ? 0 : references.size();

            List<ScholarlyArticle> citations = article.getCitations();
            int citationCount = citations == null ? 0 : citations.size();

            papers.add(new ModelPaperScholarlyArticle(
                article.getTitle(),
                authorNames,
                article.getAbstractField(),
                citationCount,
                referenceCount,
                article.getLocation()));
        }

        return papers;
    }
}
