package evolutionworks.model;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ecologylab.net.ParsedURL;
import ecologylab.semantics.generated.library.creativeWork.Author;
import ecologylab.semantics.generated.library.scholarlyPublication.ScholarlyArticle;

import evolutionworks.data.DocumentQuery;

/**
 * @author Jason Wilkins
 *
 */
class ModelPaperScholarlyArticle implements ModelPaper
{
    private String                         initTitle;
    private String                         initAbstract;
    private List<String>                   initAuthors;
    private int                            initCitationCount;
    private int                            initReferenceCount;

    private ScholarlyArticle               scholarlyArticle;

    private final ParsedURL                location;

    private ModelPaperScholarlyArticleList citations;
    private ModelPaperScholarlyArticleList references;

    private boolean fullyPopulateFailed = false;

    public ModelPaperScholarlyArticle(String title,
                                      List<String> authors,
                                      String theAbstract,
                                      int citationCount,
                                      int referenceCount,
                                      ParsedURL location)
    {
        this.location = location;

        initTitle = title;
        initAbstract = theAbstract;
        initAuthors = authors;
        initCitationCount = citationCount;
        initReferenceCount = referenceCount;
    }

    public ModelPaperScholarlyArticle(ParsedURL location)
    {
        this.location = location;

        fullyPopulate();
    }

    public void fullyPopulate()
    {
        if (!fullyPopulateFailed && scholarlyArticle == null) {
            scholarlyArticle = (ScholarlyArticle) DocumentQuery.query(location);
            
            if (scholarlyArticle != null) {
                initTitle = null;
                initAbstract = null;
                initAuthors = null;
                initCitationCount = 0;
                initReferenceCount = 0;
            }
            else {
                fullyPopulateFailed  = true;
                throw new RuntimeException("fullyPopulate Failed");
            }
        }
    }

    @Override
    public String getLocation()
    {
        if (scholarlyArticle != null) {
            ParsedURL parsedURL = scholarlyArticle.getLocation();
            
            if (parsedURL != null) {
                return parsedURL.toString();
            }
        }

        if (location != null) {
            return location.toString();
        }
        else {
            return null;
        }
    }

    @Override
    public int getCitationCount()
    {
        if (scholarlyArticle == null) {
            return initCitationCount;
        }
        else {
            List<ScholarlyArticle> cits = scholarlyArticle.getCitations();

            if (cits != null) {
                return cits.size();
            }
            else {
                return 0;
            }
        }
    }

    @Override
    public String getTitle()
    {
        if (scholarlyArticle == null) {
            return initTitle;
        }
        else {
            return scholarlyArticle.getTitle();
        }
    }

    @Override
    public int getAuthorCount()
    {
        if (scholarlyArticle == null) {
            return initAuthors == null ? 0 : initAuthors.size();
        }
        else {
            List<Author> authors = scholarlyArticle.getAuthors();
            
            if (authors != null) {
                return authors.size();
            }
            else  {
                return 0;
            }
        }
    }

    @Override
    public String getAuthor(int index)
    {
        if (scholarlyArticle == null) {
            return initAuthors.get(index);
        }
        else {
            List<Author> authors = scholarlyArticle.getAuthors();

            if (authors != null) {
                Author author = authors.get(index);
                
                if (author != null) {
                    return author.getTitle();
                }
            }
        }

        return null;
    }

    @Override
    public String getAbstract()
    {
        if (scholarlyArticle == null) {
            return initAbstract;
        }
        else {
            return scholarlyArticle.getAbstractField();
        }
    }

    @Override
    public ModelPaperList getCitations()
    {
        if (scholarlyArticle == null) {
            return null;
        }
        else {
            if (citations == null) {
                citations = new ModelPaperScholarlyArticleList(scholarlyArticle.getCitations());
            }

            return citations;
        }
    }

    @Override
    public ModelPaperList getReferences()
    {
        if (scholarlyArticle == null) {
            return null;
        }
        else {
            if (references == null) {
                references = new ModelPaperScholarlyArticleList(scholarlyArticle.getReferences());
            }

            return references;
        }
    }

    @Override
    public int getReferenceCount()
    {
        if (scholarlyArticle == null) {
            return initReferenceCount;
        }
        else {
            List<ScholarlyArticle> refs = scholarlyArticle.getReferences();

            if (refs != null) {
                return refs.size();
            }
            else {
                return 0;
            }
        }
    }

    @Override
    public Collection<String> getLinks()
    {
        Collection<String> links = new LinkedList<String>();

        if (scholarlyArticle != null) {
            addAllLinks(links, citations);
            addAllLinks(links, references);
        }

        return links;
    }

    private void addAllLinks(Collection<String> links, ModelPaperList modelPaperList)
    {
        if (modelPaperList != null) {
            for (ModelPaper link : modelPaperList) {
                String location = link.getLocation();
                
                if (location != null) {
                    links.add(location);
                }
            }
        }
    }
}
