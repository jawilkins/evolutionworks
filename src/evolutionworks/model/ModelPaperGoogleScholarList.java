package evolutionworks.model;

import java.util.ArrayList;
import java.util.List;

import ecologylab.semantics.generated.library.search.GoogleScholarSearch;
import ecologylab.semantics.generated.library.search.GoogleScholarSearchResult;

import evolutionworks.data.DocumentQuery;

/**
 * @author Jason Wilkins
 *
 */
class ModelPaperGoogleScholarList extends ModelPaperList
{
    public ModelPaperGoogleScholarList(String[] keywords)
    {
        super(init(keywords));
    }

    private static List<ModelPaper> init(String[] keywords)
    {
        GoogleScholarSearch googleScholarSearch = DocumentQuery.query(keywords);

        if (googleScholarSearch != null) {
            List<GoogleScholarSearchResult> googleScholarSearchResults = googleScholarSearch.getSearchResults();

            if (googleScholarSearchResults != null) {
                List<ModelPaper> papers = new ArrayList<ModelPaper>(googleScholarSearchResults.size());

                for (GoogleScholarSearchResult result : googleScholarSearchResults) {
                    papers.add(new ModelPaperScholarlyArticle(
                        result.getTitle(), null, null, result.getCitations(), 0, result.getLocation()));
                }

                return papers;
            }
        }

        return new ArrayList<ModelPaper>(0);
    }
}
