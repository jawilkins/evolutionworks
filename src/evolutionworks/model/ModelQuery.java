package evolutionworks.model;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import ecologylab.net.ParsedURL;

import evolutionworks.data.DocumentQuery;

import evolutionworks.model.ModelPaperTest.Corpus;

/**
 * @author Jason Wilkins
 *
 */
public final class ModelQuery
{
    private static boolean isInitialized = false;

    private static Corpus corpus;
    private static boolean initCorpus = false;

    private static Map<String, ModelPaper> paperCache = new HashMap<String, ModelPaper>();
    private static Map<String, ModelPaperList> paperListCache = new HashMap<String, ModelPaperList>();

    public static boolean isQueryCacheEnabled()
    {
        return DocumentQuery.isCacheEnabled();
    }

    public static void enableQueryCache(boolean queryCacheEnabled)
    {
        DocumentQuery.enableCache(queryCacheEnabled);
    }

    public static void initialize(boolean queryCacheEnabled)
    {
        if (!isInitialized) {
            DocumentQuery.initialize(queryCacheEnabled);

            isInitialized = true;
        }
    }

    public static void shutdown()
    {
        DocumentQuery.shutdown();
    }

    public static ModelPaperList query(String[] keywords)
    {
        String joinedKeywords = StringUtils.join(keywords, "\n");

        if (paperListCache.containsKey(joinedKeywords)) {
            return paperListCache.get(joinedKeywords);
        }
        else {
            ModelPaperList paperList = new ModelPaperGoogleScholarList(keywords);

            paperListCache.put(joinedKeywords,  paperList);

            return paperList;
        }
    }

    public static final String TEST_PREFIX = "test:";
    private static final int CORPUS_SIZE = 1000;
    private static final long CORPUS_SEED = 12345;

    public static final String HTTP_PREFIX = "http:";

    public static ModelPaper query(String url)
    {
        ModelPaper paper;

        try {
            if (paperCache.containsKey(url)) {
                paper = paperCache.get(url);
            }
            else {

                if (url.startsWith(TEST_PREFIX)) {
                    int index = Integer.parseInt(url.substring(TEST_PREFIX.length()));

                    if (!initCorpus ) {
                        corpus = new Corpus(CORPUS_SIZE, CORPUS_SEED);
                        initCorpus = true;
                    }

                    paper = new ModelPaperTest(index, corpus);
                }
                else if (url.startsWith("http:")) {
                    paper = new ModelPaperScholarlyArticle(ParsedURL.getAbsolute(url, ModelQuery.class.getName()));
                }
                else {
                    paper = null;
                }

                paperCache.put(url, paper);

                final String location = paper.getLocation();
                
                if (!url.equals(location)) {
                    paperCache.put(location, paper);
                }
            }

            paper.fullyPopulate();
        }
        catch (RuntimeException e) {
            Logger.getLogger(ModelQuery.class.getName()).log(Level.WARNING, "Query Failed: " + url, e);
            paper = new FailedPaper(url, e.toString());
        }

        return paper;
    }

    public static void setQueryTimeout(long ms)
    {
        DocumentQuery.setQueryTimeout(ms);
    }

    public static long getQueryTimeout()
    {
        return DocumentQuery.getQueryTimeout();
    }

    /**
     * private constructor because utility classes should not have instances
     */
    private ModelQuery() {}
}
