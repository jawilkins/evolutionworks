package evolutionworks.model;

public interface ModelCard
{
    boolean setPosition(int x, int y);
    boolean setSize(int width, int height);
    void setVisible(boolean visible);
    void close();
}
