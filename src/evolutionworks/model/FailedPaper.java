package evolutionworks.model;

public class FailedPaper implements ModelPaper
{
    private final String url;
    private final String message;

    public FailedPaper(String url, String message)
    {
        assert url     != null;
        assert message != null;

        this.url     = url;
        this.message = message;
    }

    @Override
    public String getLocation()
    {
        return url;
    }

    @Override
    public int getCitationCount()
    {
        return 0;
    }

    @Override
    public int getReferenceCount()
    {
        return 0;
    }

    @Override
    public String getTitle()
    {
        return "Query Failed: " + url;
    }

    @Override
    public int getAuthorCount()
    {
        return 0;
    }

    @Override
    public String getAuthor(int index)
    {
        return null;
    }

    @Override
    public String getAbstract()
    {
        return message;
    }

    @Override
    public ModelPaperList getCitations()
    {
        return null;
    }

    @Override
    public ModelPaperList getReferences()
    {
        return null;
    }

    @Override
    public java.util.Collection<String> getLinks()
    {
        return new java.util.LinkedList<String>();
    }

    @Override
    public void fullyPopulate()
    {
    }
}
