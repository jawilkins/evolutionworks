package evolutionworks;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import ecologylab.net.ParsedURL;
import ecologylab.semantics.generated.library.scholarlyPublication.ScholarlyArticle;
import ecologylab.semantics.generated.library.search.GoogleScholarSearch;
import ecologylab.semantics.generated.library.search.GoogleScholarSearchResult;
import evolutionworks.data.DocumentQuery;

/**
* @author gaurav_kl
* main application class for the EvolutionWorks web spider 
*/
public final class Spider
{
    private static Map<String, Object> requested              = new LinkedHashMap<String, Object>();
    private static Queue<String>       queue                  = new LinkedList<String>();

    private static int                 successCount           = 0;
    private static int                 enqueuedCount          = 0;

    private static int                 softLimit              = 0;

    private static final Logger        LOGGER                 = Logger.getLogger(Spider.class.getName());

    private static final String        REQUESTED_FILENAME     = "requested.txt";
    private static final String        QUEUE_FILENAME         = "queue.txt";
    private static final String        SUCCESS_COUNT_FILENAME = "successCount.txt";

    /**
    * main application entry point for the EvolutionWorks web spider
    * @param args
    */
    public static void main(String[] args) throws IOException
    {
        LOGGER.setLevel(Level.INFO);

        if (args.length == 0) {
            throw new IllegalArgumentException("Usage: spider softLimit [keywords]\nIf no keywords are given then the spider will try to resume previous session.");
        }

        softLimit = Integer.parseInt(args[0]);

        boolean resume = initialize(args);

        crawl(resume);
    }

    public static boolean initialize(String[] args) throws IOException
    {
        DocumentQuery.initialize(true);

        DocumentQuery.setQueryTimeout(0);

        boolean resume = (args.length == 1);

        if (resume) {
            initResume();
        }
        else {
            String[] keywords = new String[args.length - 1];

            for (int i = 1; i < args.length; i++) {
                keywords[i-1] = "\"" + args[i] + "\"";    
            }

            initSearch(keywords);
        }

        return resume;
    }

    public static void crawl(boolean resume) throws IOException
    {
        BufferedWriter requestedFile = null;
        try {
            requestedFile = new BufferedWriter(new FileWriter(REQUESTED_FILENAME, resume));

            while (!queue.isEmpty()) {
                String url = queue.remove();

                saveQueue();

                if (needRequest(url)) {
                    ScholarlyArticle article = makeRequest(requestedFile, url);

                    if (article != null) {
                        successCount++;

                        BufferedWriter queuedCountFile = null;
                        try {
                            queuedCountFile = new BufferedWriter(new FileWriter(SUCCESS_COUNT_FILENAME, false));
                            queuedCountFile.write(Integer.toString(successCount));
                        }
                        finally {
                            if (queuedCountFile != null) {
                                queuedCountFile.close();
                                queuedCountFile = null;
                            }
                        }

                        LOGGER.info("Articles Retrieved: " + successCount + "/" + softLimit);

                        if (enqueuedCount < softLimit) {
                            queueArticles(article.getCitations());
                            queueArticles(article.getReferences());
                        }
                    }
                    else {
                        enqueuedCount--;
                    }
                }
            }
        }
        finally {
            if (requestedFile != null) {
                requestedFile.close();
                requestedFile = null;
            }
        }
    }

    /**
     * @param keywords
     * @throws IOException
     * get the google scholar results for the keywords and add them to the queue
     */
    private static void initSearch(String[] keywords) throws IOException
    {
        GoogleScholarSearch googleScholarSearch = DocumentQuery.query(keywords);
        List<GoogleScholarSearchResult> results = googleScholarSearch.getSearchResults();

        for (int i = 0; i < results.size(); i++) {
            queueLocation(results.get(i).getLocation().toString());
        }

        // requested list remains empty

        // success count remains 0
    }

    private static void initResume() throws IOException
    {
        BufferedReader queueFile = null;
        try {
            queueFile = new BufferedReader(new FileReader(QUEUE_FILENAME));
            while (true) {
                String url = queueFile.readLine();

                if (url == null) {
                    break;
                }

                queueLocation(url);
            }
        }
        finally {
            if (queueFile != null) {
                queueFile.close();
                queueFile = null;
            }
        }

        BufferedReader requestedFile = null;
        try {
            requestedFile = new BufferedReader(new FileReader(REQUESTED_FILENAME));
            while (true) {
                String url = requestedFile.readLine();

                if (url == null) {
                    break;
                }

                requested.put(url, null);
            }
        }
        finally {
            if (requestedFile != null) {
                requestedFile.close();
                requestedFile = null;
            }
        }

        BufferedReader successCountFile = null;
        try {
            successCountFile = new BufferedReader(new FileReader(SUCCESS_COUNT_FILENAME));

            String line = successCountFile.readLine();

            if (line != null) {
                successCount = Integer.parseInt(line);
            }
            else {
                throw new IOException("failure reading " + SUCCESS_COUNT_FILENAME);
            }
        }
        finally {
            if (successCountFile != null) {
                successCountFile.close();
                successCountFile = null;
            }
        }

        enqueuedCount += successCount;
    }

    private static ScholarlyArticle makeRequest(BufferedWriter requestedFile, String url) throws IOException
    {
        requested.put(url, null);

        requestedFile.write(url);
        requestedFile.newLine();
        requestedFile.flush();

        return (ScholarlyArticle) DocumentQuery.query(ParsedURL.getAbsolute(url, "makeRequest"));
    }

    private static boolean needRequest(String url)
    {
        return !requested.containsKey(url);
    }

    /**
     * @throws IOException
     * save queue in case of failure
     */
    private static void saveQueue() throws IOException
    {
        BufferedWriter queueFile = null;
        try {
            queueFile = new BufferedWriter(new FileWriter(QUEUE_FILENAME, false));

            for (String entry : (LinkedList<String>) queue) {
                queueFile.write(entry);
                queueFile.newLine();
                queueFile.flush();
            }
        }
        finally {
            if (queueFile != null) {
                queueFile.close();
                queueFile = null;
            }
        }
    }

    /**
     * @param location
     * @throws IOException
     */
    private static void queueLocation(String location) throws IOException
    {
        if (needRequest(location)) {
            queue.add(location);
            enqueuedCount++;
        }
    }

    /**
     * @param articles
     * @throws IOException
     */
    private static void queueArticles(List<ScholarlyArticle> articles) throws IOException
    {
        if (articles != null) {
            for (ScholarlyArticle article : articles) {
                ParsedURL location = article.getLocation();

                if (location != null) {
                    queueLocation(location.toString());
                }
                // else, we have to skip articles that don't have valid locations
            }
        }
    }

    /**
    * private constructor because utility classes should not have instances
    */
    private Spider()
    {}
}
