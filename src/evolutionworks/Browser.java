package evolutionworks;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;

import evolutionworks.model.ModelQuery;
import evolutionworks.ui.MainWindow;

/**
 * @author Ajit Jain
 * main application class for the EvolutionWorks browser 
 */
public final class Browser
{
    private static String userName;
    private static String machineName;
    private static int runId;
    private static boolean isTraditional = false;
    private static boolean isUserTest = false;
    private static String testKeywords = "human centered computing";
    private static int readingListCount;
    private static BufferedWriter logFile;
    private static boolean testComplete = false;

    private static final Logger LOGGER = Logger.getLogger(Browser.class.getName());
    
    private static StopWatch testTimeStopWatch = new StopWatch();

    private static boolean stopwatchStarted;

    private static final boolean DEFAULT_QUERY_CACHE_ENABLED = true;

    /**
     * main application entry point for the EvolutionWorks browser
     * @param args
     */
    public static void main(String[] args)
    {
        if (args.length != 5) {
            throw new IllegalArgumentException("usage: userName machineName [browser|graph] \"keywords to search\" readlingListCount");
        }

        isUserTest = false;

        userName = args[0];

        machineName = args[1];

        if (args[2].equals("browser")) {
            isTraditional = true;
        }
        else if (args[2].equals("graph")) {
            isTraditional = false;
        }
        else {
            throw new IllegalArgumentException("test type must be set to 'browser' or 'graph'");
        }

        testKeywords = args[3];

        readingListCount = Integer.parseInt(args[4]);

        BufferedReader runIdFileIn = null;
        try {
            runIdFileIn = new BufferedReader(new FileReader("runid.txt"));
            runId = Integer.parseInt(runIdFileIn.readLine());
        }
        catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            runId = 0;
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            BufferedWriter runIdFileOut = null;
            try {
                if (runIdFileIn != null) {
                    runIdFileIn.close();
                }

                runIdFileOut = new BufferedWriter(new FileWriter("runid.txt", false));
                runIdFileOut.write(Integer.toString(runId + 1));
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            finally {
                if (runIdFileOut != null) {
                    try {
                        runIdFileOut.close();
                    }
                    catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }

        try {
            logFile = new BufferedWriter(new FileWriter("log.csv", true));
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            logEvent("initialzing");
            ModelQuery.initialize(DEFAULT_QUERY_CACHE_ENABLED);

            logEvent("opening main window");
            MainWindow.show();

            ModelQuery.shutdown();
        }
        catch (RuntimeException e) {
            e.printStackTrace();
        }
        finally {
            try {
                logFile.close();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        System.exit(0);
    }

    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";

    public static String now() {
      Calendar cal = Calendar.getInstance();
      SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
      return sdf.format(cal.getTime());
    }

    public static void logEvent(String message)
    {
        logEvent("event", message);
    }

    public static void logVariable(String varName, Object var)
    {
        testLog("variable", varName, var.toString());
    }

    public static void logTimer(String timerName, long interval)
    {
        testLog("timer", timerName, Long.toString(interval));
    }

    public static void testLog(String messageType, String messageSubType, String messageContent)
    {
        List<String> csv = new LinkedList<String>();

        csv.add(StringEscapeUtils.escapeCsv(Integer.toString(runId)));
        csv.add(StringEscapeUtils.escapeCsv(now()));
        csv.add(StringEscapeUtils.escapeCsv(userName));
        csv.add(StringEscapeUtils.escapeCsv(machineName));
        csv.add(StringEscapeUtils.escapeCsv(Boolean.toString(isTraditional)));
        csv.add(StringEscapeUtils.escapeCsv(Boolean.toString(isUserTest)));
        csv.add(StringEscapeUtils.escapeCsv(testKeywords));
        csv.add(StringEscapeUtils.escapeCsv(Integer.toString(readingListCount)));
        csv.add(StringEscapeUtils.escapeCsv(stopwatchStarted ? Long.toString(testTimeStopWatch.getStartTime()) : "-1"));
        csv.add(StringEscapeUtils.escapeCsv(stopwatchStarted ? Long.toString(testTimeStopWatch.getTime()) : "-1"));
        csv.add(StringEscapeUtils.escapeCsv(messageType));
        csv.add(StringEscapeUtils.escapeCsv(messageSubType));
        csv.add(StringEscapeUtils.escapeCsv(messageContent));

        try {
            final String record = StringUtils.join(csv, ", ");

            logFile.write(record);
            logFile.newLine();
            logFile.flush();

            LOGGER.info(record);
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * private constructor because utility classes should not have instances
     */
    private Browser()
    {}

    public static boolean isTraditional()
    {
        return isTraditional ;
    }

    public static boolean isUserTest()
    {
        return isUserTest;
    }

    public static String getTestKeywords()
    {
        return testKeywords;
    }

    public static void startTest()
    {
        logEvent("starting main timer");
        testTimeStopWatch.start();
        stopwatchStarted = true;
    }

    public static void endTest()
    {
        testTimeStopWatch.stop();
        stopwatchStarted = false;
        logEvent("stopped main timer");
        logTimer("total", testTimeStopWatch.getTime());
        testComplete = true;
    }

    public static void changeReadingListCount(Collection<String> readingList)
    {
        testLog("reading-list", "size", Integer.toString(readingList.size()));

        int i = 0;
        for (String url : readingList) {
            testLog("reading-list", Integer.toString(i++), url);
        }
    }

    public static void logEvent(String subEvent, String message)
    {
        testLog("event", subEvent, message);
    }

    public static int getReadingListCount()
    {
        return readingListCount;
    }

    public static boolean isTestCompleted()
    {
        return testComplete;
    }
}
