package evolutionworks.layout;

import java.util.Collection;
import java.util.Random;

import evolutionworks.model.ModelArrowRenderer;
import evolutionworks.model.ModelCard;

/**
 * @author Jason Wilkins
 *
 */
public class LayoutUtils
{
    static final Random RANDOM = new Random(122079);
    static final double DISTANCE_FROM_PARENT = 0.2;

    public static CardNode newCardNode(
        NetworkBuilder networkBuilder,
        String key,
        Collection<String> links,
        ModelCard cardWidget,
        ModelArrowRenderer modelArrowRenderer,
        String parentKey,
        double windowX,
        double windowY,
        double windowWidth,
        double windowHeight,
        double width,
        double height)
    {
        CardNode parentCardNode;

        parentCardNode = (parentKey == null) ? null : networkBuilder.getCardNode(parentKey);

        double x;
        double y;

        if (parentCardNode == null) {
            x = windowWidth * RANDOM.nextDouble() + windowX;
            y = windowHeight * RANDOM.nextDouble() + windowY;
        }
        else {
            double angle = 2 * RANDOM.nextDouble() * Math.PI;
            double diag = DISTANCE_FROM_PARENT * Math.max(parentCardNode.getDiagonal(), Math.sqrt(width*width + height*height));
            x = parentCardNode.getCenterX() + diag * Math.cos(angle);
            y = parentCardNode.getCenterY() + diag * Math.sin(angle);
        }

        CardNode cardNode = new CardNode(key, cardWidget, x - width / 2, y - height / 2, width, height);

        networkBuilder.addNode(cardNode, links, modelArrowRenderer);

        return cardNode;
    }

    /**
     * utility class private constructor
     */
    private LayoutUtils() {}
}
