package evolutionworks.layout;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.GraphView;
import org.gephi.graph.api.Node;
import org.gephi.graph.api.NodeData;

import org.openide.util.Lookup;

/**
 * @author Jason Wilkins
 *
 */
public class World
{
    public World()
    {
        graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
        graphView  = graphModel.getVisibleView();
        graph      = graphModel.getDirectedGraph();
    }

    public Collection<CardNode> getCardNodes()
    {
        return cardNodes;
    }

    public Collection<CardEdge> getCardEdges()
    {
        return cardEdges;
    }

    public void addNode(CardNode cardNode)
    {
        if (!cardNodes.contains(cardNode)) {
            if (cardNode.getNode() != null) {
                throw new IllegalArgumentException("cardNode already inserted into a different graph");
            }

            cardNode.setNode(graphModel.factory().newNode());

            graph.addNode(cardNode.getNode());

            cardNodes.add(cardNode);

            Point center = cardNode.getRectangle().getCenter();

            NodeData nodeData = cardNode.getNode().getNodeData();
            nodeData.setX((int) (center.getX()));
            nodeData.setY((int) (center.getY()));
            nodeData.setSize((int) (cardNode.getDiagonal() / 2));
        }
    }

    public void removeNode(CardNode cardNode)
    {
        if (cardNodes.remove(cardNode)) {
            Node node = cardNode.getNode();

            if (node != null) {
                graph.removeNode(node);
            }

            cardNode.setNode(null);
            cardNode.getWidget().close();
        }
    }

    public void addEdge(CardEdge cardEdge)
    {
        if (!cardEdges.contains(cardEdge)) {
            if (cardEdge.getSourceNode() == null) {
                throw new IllegalArgumentException("cardEdge.sourceNode cannot be null");
            }

            if (cardEdge.getTargetNode() == null) {
                throw new IllegalArgumentException("cardEdge.targetNode cannot be null");
            }

            addNode(cardEdge.getSourceNode());
            addNode(cardEdge.getTargetNode());

            cardEdge.setEdge(graphModel.factory().newEdge(
                cardEdge.getSourceNode().getNode(),
                cardEdge.getTargetNode().getNode()));

            graph.addEdge(cardEdge.getEdge());

            cardEdges.add(cardEdge);
        }
    }

    public void removeEdge(CardEdge cardEdge)
    {
        if (cardEdges.remove(cardEdge)) {
            graph.removeEdge(cardEdge.getEdge());
            cardEdge.setEdge(null);
        }
    }

    public void setActive()
    {
        graphModel.setVisibleView(graphView);
    }

    /**
     * @return the model
     */
    GraphModel getGraphModel()
    {
        return graphModel;
    }

    private Set<CardNode> cardNodes = new LinkedHashSet<CardNode>();

    private Set<CardEdge> cardEdges = new LinkedHashSet<CardEdge>();

    private GraphModel graphModel;
    private GraphView  graphView;
    private Graph      graph;
}
