package evolutionworks.layout;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import evolutionworks.model.ModelArrowRenderer;

public class NetworkBuilder
{
    private final World world = new World();

    private Map<String, CardNode> cardNodeMap = new LinkedHashMap<String, CardNode>();
    private Map<String, CardEdge> cardEdgeMap = new LinkedHashMap<String, CardEdge>();

    public void addNode(CardNode cardNode, Collection<String> links, ModelArrowRenderer modelArrowRenderer)
    {
        final String key = cardNode.getKey();

        if (cardNodeMap.containsKey(key)) {
            CardNode oldNode = cardNodeMap.get(key);
            removeNode(oldNode);
        }

        if (links != null) {
            for (String link : links) {
                if (cardNodeMap.containsKey(link)) {
                    final CardNode otherCardNode = cardNodeMap.get(link);
                    final String otherKey = otherCardNode.getKey();

                    CardNode a;
                    CardNode b;

                    String keyA;
                    String keyB;

                    if (key.compareTo(otherKey) <= 0) {
                        a = cardNode;
                        b = otherCardNode;

                        keyA = key;
                        keyB = otherKey;
                    }
                    else {
                        a = otherCardNode;
                        b = cardNode;

                        keyA = otherKey;
                        keyB = key;
                    }

                    CardEdge cardEdge = new CardEdge(a, b, modelArrowRenderer);

                    final String cardEdgeKey = keyA + '\n' + keyB;

                    cardEdgeMap.put(cardEdgeKey, cardEdge);
                    world.addEdge(cardEdge);
                }
            }
        }

        cardNodeMap.put(key, cardNode);
        world.addNode(cardNode);
    }

    public void removeNode(CardNode cardNode)
    {
        final String cardNodeKey = cardNode.getKey();
        final Collection<String> toRemove = new LinkedList<String>();

        for (Entry<String, CardEdge> entry : cardEdgeMap.entrySet()) {
            CardEdge cardEdge = entry.getValue();
            if (cardEdge.getSourceNode().getKey() == cardNodeKey ||
                cardEdge.getTargetNode().getKey() == cardNodeKey)
            {
                toRemove.add(entry.getKey()); // cannot remove while iterating, so save key
                world.removeEdge(cardEdge);
            }
        }
        
        // remove the entries that were found
        for (String key : toRemove) {
            cardEdgeMap.remove(key);
        }

        cardNodeMap.remove(cardNodeKey);
        world.removeNode(cardNode);
    }

    public CardNode getCardNode(String parentKey)
    {
        return cardNodeMap.get(parentKey);
    }

    /**
     * @return the world
     */
    public World getWorld()
    {
        return world;
    }
}
