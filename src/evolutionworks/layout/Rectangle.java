package evolutionworks.layout;

public class Rectangle
{
    Rectangle(double x, double y, double width, double height)
    {
        this.x      = x;
        this.y      = y;
        this.width  = width;
        this.height = height;
    }

    Rectangle(Rectangle original)
    {
        copy(original);
    }

    Rectangle()
    {
        x      = 0;
        y      = 0;
        width  = 0;
        height = 0;
    }

    public final void copy(Rectangle original)
    {
        x      = original.x;
        y      = original.y;
        width  = original.width;
        height = original.height;
    }

    void setBetween(Rectangle r0, Rectangle r1, double t)
    {
        x      = lerp(r0.x,      r1.x,      t);
        y      = lerp(r0.y,      r1.y,      t);
        width  = lerp(r0.width,  r1.width,  t);
        height = lerp(r0.height, r1.height, t);
    }

    void union(Rectangle rectangle)
    {
        double newX = x < rectangle.x ? x : rectangle.x;
        double newY = y < rectangle.y ? y : rectangle.y;

        double thisXMax = x + width;
        double thisYMax = y + height;

        double rectangleXMax = rectangle.x + rectangle.width;
        double rectangleYMax = rectangle.y + rectangle.height;

        double newXMax = thisXMax > rectangleXMax ? thisXMax : rectangleXMax;
        double newYMax = thisYMax > rectangleYMax ? thisYMax : rectangleYMax;

        x      = newX;
        y      = newY;
        width  = newXMax - x;
        height = newYMax - y;
    }

    private double lerp(double a, double b, double t)
    {
        return a + t*(b - a);
    }

    Point getCenter()
    {
        return new Point(x + width / 2, y + height / 2);
    }

    boolean isIntersecting(Rectangle other)
    {
        return !((x > other.x + other.width) || (y > other.y + other.height) || (x + width < other.x) || (y + height < other.y));
    }

    void adjustAspectRatio(double aspectRatio)
    {
        double newWidth;
        double newHeight;

        if (aspectRatio > width / height) {
            newWidth  = height * aspectRatio;
            newHeight = height;
        }
        else {
            newWidth  = width;
            newHeight = width / aspectRatio;
        }

        x += (width  - newWidth) / 2;
        y += (height - newHeight) / 2;

        width  = newWidth;
        height = newHeight;
    }

    public double getDiagonal()
    {
        return Math.sqrt(width * width + height * height);
    }

    public double getX()
    {
        return x;
    }

    public void setX(double x)
    {
        this.x = x;
    }

    public double getY()
    {
        return y;
    }

    public void setY(double y)
    {
        this.y = y;
    }

    public double getWidth()
    {
        return width;
    }

    public void setWidth(double width)
    {
        this.width = width;
    }

    public double getHeight()
    {
        return height;
    }

    public void setHeight(double height)
    {
        this.height = height;
    }

    public void setPosition(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public void setSize(double width, double height)
    {
        this.width  = width;
        this.height = height;
    }

    private double x, y, width, height;
}
