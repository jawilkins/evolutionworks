package evolutionworks.layout;

import java.util.logging.Level;
import java.util.logging.Logger;

//import org.gephi.layout.plugin.force.yifanHu.YifanHuLayout;
//import org.gephi.layout.plugin.force.StepDisplacement;

import org.gephi.layout.plugin.forceAtlas.ForceAtlas;
import org.gephi.layout.plugin.forceAtlas.ForceAtlasLayout;

public class Animator
{
    public Animator()
    {
        //CardLayoutBuilder builder = new CardLayoutBuilder();
        //layout  = new CardLayout(builder);

        //layout = new YifanHuLayout(null, new StepDisplacement(1f));

        //ForceAtlas2Builder builder = new ForceAtlas2Builder();
        //layout = builder.buildLayout();

        ForceAtlas builder = new ForceAtlas();
        layout = builder.buildLayout();
    }

    private static final double REPULSION_STRENGTH = 1000.0;
    
    public void enableCollisions(boolean enabled)
    {
        layout.setAdjustSizes(enabled);	    
    }

    public void initAnimation(World world)
    {
        Logger log = Logger.getLogger("evolutionworks.layout.Animator");

        try {
            layout.setGraphModel(world.getGraphModel());
            layout.initAlgo();
            world.setActive();
            layout.resetPropertiesValues();

            //layout.setOptimalDistance(500f);
            layout.setSpeed(0.0025);
            layout.setAdjustSizes(true);
            layout.setRepulsionStrength(REPULSION_STRENGTH);
        } catch (Exception e) {
            log.log(Level.SEVERE, "Layout algorithm failure", e);
        }
    }

    private int steps = 0;
    
    public void primeAnimation(int steps)
    {
        this.steps = steps;
    }

    public boolean stepAnimation()
    {
        if (steps-- > 0 && layout.canAlgo()) {
            layout.goAlgo();

            return true;
        }
        else {
            return false;
        }
    }

    public void endAnimation()
    {
        layout.endAlgo();
    }

    //private CardLayout layout;
    //private YifanHuLayout layout;
    private ForceAtlasLayout layout;
    //private ForceAtlas2 layout;
}
