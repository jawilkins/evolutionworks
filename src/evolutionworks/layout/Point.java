package evolutionworks.layout;

public class Point
{
    Point()
    {
        this.x = 0;
        this.y = 0;
    }

    Point(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    Point newFromTo(Point to)
    {
        return new Point(to.x - x, to.y - y);
    }

    Point newScale(double scale)
    {
        return new Point(scale*x, scale*y);
    }

    void setX(double x)
    {
        this.x = x;
    }

    void setY(double y)
    {
        this.y = y;
    }

    private double x, y;
}
