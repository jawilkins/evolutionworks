package evolutionworks.layout;

import org.gephi.graph.api.Edge;

import evolutionworks.model.ModelArrowRenderer;

public class CardEdge
{
    CardEdge(CardNode sourceNode, CardNode targetNode, ModelArrowRenderer arrowRenderer)
    {
        this.sourceNode = sourceNode;
        this.targetNode = targetNode;

        this.arrowRenderer = arrowRenderer;
    }

    public Point getSourcePoint()
    {
        // TODO: point to edge of card instead of center
        return getSourceNode().getCenter();
    }

    public Point getTargetPoint()
    {
        // TODO: point to edge of card instead of center
        return getTargetNode().getCenter();
    }

    public double getWeight()
    {
        // TODO: this is just a placeholder
        return 1;
    }

    CardNode getSourceNode()
    {
        return sourceNode;
    }

    CardNode getTargetNode()
    {
        return targetNode;
    }

    Edge getEdge()
    {
        return edge;
    }

    void setEdge(Edge edge)
    {
        this.edge = edge;
    }

    ModelArrowRenderer getArrowRenderer()
    {
        return arrowRenderer;
    }

    private CardNode           sourceNode;
    private CardNode           targetNode;

    private Edge               edge;

    private ModelArrowRenderer arrowRenderer;
}
