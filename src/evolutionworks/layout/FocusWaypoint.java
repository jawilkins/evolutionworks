package evolutionworks.layout;

public class FocusWaypoint
{
    private final Rectangle rectangle;
    private final double    smooth;
    private final double    margin;
    private final int       steps;

    public FocusWaypoint(CardNode cardNode, double smooth, double margin, int steps)
    {
        this(cardNode.getRectangle(), smooth, margin, steps);
    }

    public FocusWaypoint(Rectangle rectangle, double smooth, double margin, int steps)
    {
        assert rectangle != null;

        this.rectangle = rectangle;
        this.smooth    = smooth;
        this.margin    = margin;
        this.steps     = steps;
    }

    /**
     * @return the steps
     */
    public int getSteps()
    {
        return steps;
    }

    /**
     * @return the focus
     */
    public Rectangle getRectangle()
    {
        return rectangle;
    }

    public double getSmooth()
    {
        return smooth;
    }

    public double getMargin()
    {
        return margin;
    }
}
