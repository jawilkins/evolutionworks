package evolutionworks.layout;

import org.gephi.layout.plugin.forceAtlas2.ForceAtlas2Builder;
import org.gephi.layout.spi.LayoutBuilder;

import org.openide.util.lookup.ServiceProvider;

@ServiceProvider(service = LayoutBuilder.class)
public class CardLayoutBuilder implements LayoutBuilder
{
    @Override
    public CardLayout buildLayout()
    {
        return new CardLayout(this);
    }

    @Override
    public String getName()
    {
        return "EvolutionWorks";
    }

    @Override
    public CardLayoutUI getUI()
    {
        return ui;
    }

    ForceAtlas2Builder getForceAtlas2Builder()
    {
        return forceAtlas2Builder;
    }

    void setForceAtlas2Builder(ForceAtlas2Builder forceAtlas2Builder)
    {
        this.forceAtlas2Builder = forceAtlas2Builder;
    }

    private static CardLayoutUI ui = new CardLayoutUI();

    private ForceAtlas2Builder forceAtlas2Builder = new ForceAtlas2Builder();
}
