package evolutionworks.layout;

import org.gephi.graph.api.Node;

import evolutionworks.model.ModelCard;

public class CardNode
{
    private final String    key;

    private final Rectangle rectangle;

    private final ModelCard cardWidget;

    private Node            node;

    CardNode(String key, ModelCard cardWidget, double x, double y, double width, double height)
    {
        assert key        != null;
        assert cardWidget != null;

        this.key = key;
        this.cardWidget = cardWidget;

        rectangle = new Rectangle(x, y, width, height);
    }

    public void setSize(double width, double height)
    {
        rectangle.setSize(width, height);
    }

    Point getCenter()
    {
        return rectangle.getCenter();
    }

    public double getCenterX()
    {
        return rectangle.getX() + (rectangle.getWidth() / 2);
    }

    public double getCenterY()
    {
        return rectangle.getY() + (rectangle.getHeight() / 2);
    }

    void setCenter(double newX, double newY)
    {
        rectangle.setX(newX  -  rectangle.getWidth()  / 2);
        rectangle.setY(newY  -  rectangle.getHeight() / 2);
    }

    public double getDiagonal()
    {
        return rectangle.getDiagonal();
    }

    Rectangle getRectangle()
    {
        return rectangle;
    }

    /**
     * @return the widget
     */
    ModelCard getWidget()
    {
        return cardWidget;
    }

    /**
     * @return the node
     */
    Node getNode()
    {
        return node;
    }

    /**
     * @param node the node to set
     */
    void setNode(Node node)
    {
        this.node = node;
    }

    /**
     * @return the key
     */
    public String getKey()
    {
        return key;
    }

    public double getX()
    {
        return rectangle.getX();
    }

    public double getY()
    {
        return rectangle.getY();
    }

    public double getWidth()
    {
        return rectangle.getWidth();
    }

    public double getHeight()
    {
        return rectangle.getHeight();
    }
}
