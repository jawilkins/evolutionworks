package evolutionworks.layout;

import javax.swing.Icon;
import javax.swing.JPanel;

import org.gephi.layout.spi.Layout;
import org.gephi.layout.spi.LayoutUI;

class CardLayoutUI implements LayoutUI
{
    @Override
    public String getDescription()
    {
        return "EvolutionWorks reference card graph layout algorithm";
    }

    @Override
    public Icon getIcon()
    {
        return null;
    }

    @Override
    public JPanel getSimplePanel(Layout layout)
    {
        return null;
    }

    @Override
    public int getQualityRank()
    {
        return -1;
    }

    @Override
    public int getSpeedRank()
    {
        return -1;
    }
}
