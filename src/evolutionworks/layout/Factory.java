package evolutionworks.layout;

import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;

public class Factory
{
    public static NetworkBuilder getNetworkBuilder()
    {
        initialize();

        return new NetworkBuilder();
    }

    private static void initialize()
    {
        if (!initialized) {
            pc = Lookup.getDefault().lookup(ProjectController.class);
            pc.newProject();
            workspace = pc.getCurrentWorkspace();

            initialized = true;
        }
    }

    static ProjectController getProjectController()
    {
        initialize();

        return pc;
    }

    static Workspace getWorkspace()
    {
        initialize();

        return workspace;
    }

     /**
     * utility class private constructor
     */
    private Factory() {}

    private static boolean           initialized = false;

    private static ProjectController pc;
    private static Workspace         workspace;
}
