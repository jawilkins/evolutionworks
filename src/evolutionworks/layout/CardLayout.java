package evolutionworks.layout;

import java.util.ArrayList;

import org.gephi.graph.api.GraphModel;
import org.gephi.layout.plugin.forceAtlas2.ForceAtlas2;
import org.gephi.layout.spi.Layout;
import org.gephi.layout.spi.LayoutProperty;

public class CardLayout implements Layout
{
    CardLayout(CardLayoutBuilder builder)
    {
        this.builder = builder;

        this.forceAtlas2 = new ForceAtlas2(builder.getForceAtlas2Builder());
    }

    @Override
    public boolean canAlgo()
    {
        return forceAtlas2.canAlgo();
    }

    @Override
    public void endAlgo()
    {
        forceAtlas2.endAlgo();
    }

    @Override
    public CardLayoutBuilder getBuilder()
    {
        return builder;
    }

    @Override
    public LayoutProperty[] getProperties()
    {
        ArrayList<LayoutProperty> properties = new ArrayList<LayoutProperty>();

        int size = properties.size();

        return properties.toArray(new LayoutProperty[size]);
    }

    @Override
    public void goAlgo()
    {
        forceAtlas2.goAlgo();
    }

    @Override
    public void initAlgo()
    {
        forceAtlas2.initAlgo();
    }

    @Override
    public void resetPropertiesValues()
    {
        forceAtlas2.resetPropertiesValues();
        forceAtlas2.setAdjustSizes(true);
    }

    @Override
    public void setGraphModel(GraphModel graphModel)
    {
        forceAtlas2.setGraphModel(graphModel);
        resetPropertiesValues();
    }

    private CardLayoutBuilder builder;

    private ForceAtlas2 forceAtlas2;
}
