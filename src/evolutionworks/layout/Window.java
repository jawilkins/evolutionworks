package evolutionworks.layout;

import java.util.Queue;

import org.gephi.graph.api.NodeData;

import evolutionworks.model.ModelArrowRenderer;

public class Window
{
    private double focusSmooth;
    private double focusMargin;
    private int    focusSteps;

    public Window()
    {
        bounds   = new Rectangle(0, 0, 1, 1);
        bounds0  = new Rectangle(bounds);
        bounds1  = new Rectangle(bounds);
        viewport = new Rectangle(bounds);
    }

    public void renderArrows(World world)
    {
        Iterable<CardEdge> cardEdges = world.getCardEdges();

        for (CardEdge cardEdge : cardEdges) {
            Point sourcePoint = newViewportFromWorld(cardEdge.getSourcePoint());
            Point targetPoint = newViewportFromWorld(cardEdge.getTargetPoint());

            cardEdge.getArrowRenderer().render(
                sourcePoint.getX(),
                sourcePoint.getY(),
                targetPoint.getX(),
                targetPoint.getY(),
                (viewport.getDiagonal() / bounds.getDiagonal()) * cardEdge.getWeight());
        }
    }

    private Point newViewportFromWorld(Point worldPoint)
    {
        return new Point(transformWorldToViewportX(worldPoint.getX()), transformWorldToViewportY(worldPoint.getY()));
    }

    public double transformWorldToViewportX(double worldX)
    {
        return viewport.getX() + (worldX - bounds.getX()) * getWorldToViewportRatioWidth();
    }

    public double transformWorldToViewportY(double worldY)
    {
        return viewport.getY() + (worldY - bounds.getY()) * getWorldToViewportRatioHeight();
    }

    public double getWorldToViewportRatioWidth()
    {
        return viewport.getWidth() / bounds.getWidth();
    }

    public double getWorldToViewportRatioHeight()
    {
        return viewport.getHeight() / bounds.getHeight();
    }

    public void positionWidgets(World world)
    {
        for (CardNode cardNode : world.getCardNodes()) {
            cardNode.getWidget().setVisible(false);

            NodeData nodeData = cardNode.getNode().getNodeData();

            cardNode.setCenter(nodeData.x(), nodeData.y());

            if (isRectangleVisible(cardNode.getRectangle())) {
                cardNode.getWidget().setVisible(true);

                Rectangle viewportRectangle = newViewportFromWorld(cardNode.getRectangle());

                cardNode.getWidget().setPosition(
                    (int)viewportRectangle.getX(),
                    (int)viewportRectangle.getY());

                cardNode.getWidget().setSize(
                    Math.max(0, (int)viewportRectangle.getWidth()),
                    Math.max(0, (int)viewportRectangle.getHeight()));
            }
        }
    }

    private Rectangle newViewportFromWorld(Rectangle worldRectangle)
    {
        return new Rectangle(transformWorldToViewportX(worldRectangle.getX()),
                             transformWorldToViewportY(worldRectangle.getY()),
                             worldRectangle.getWidth()  * getWorldToViewportRatioWidth(),
                             worldRectangle.getHeight() * getWorldToViewportRatioHeight());
    }

    private boolean isRectangleVisible(Rectangle worldRectangle)
    {
        return worldRectangle.isIntersecting(bounds);
    }

    public void setViewport(double x, double y, double width, double height)
    {
        viewport.setX(x);
        viewport.setY(y);
        viewport.setWidth(width);
        viewport.setHeight(height);
    }

    public void setBoundsFromViewport(double x, double y, double width, double height, double margin, int steps)
    {
        Rectangle newBounds = new Rectangle(transformViewportToWorldX(x, bounds) - getViewportToWorldRatioWidth(bounds)*margin, transformViewportToWorldY(y, bounds) - getViewportToWorldRatioHeight(bounds)*margin, getViewportToWorldRatioWidth(bounds)*(width + 2 * margin), getViewportToWorldRatioHeight(bounds)*(height + 2 * margin));

        initAnimation(newBounds, steps);
    }

    public void setBounds(double x, double y, double width, double height, double margin, int steps)
    {
        Rectangle newBounds = new Rectangle(x - margin, y - margin, width + 2 * margin, height + 2 * margin);

        initAnimation(newBounds, steps);
    }

    public void setBounds(Rectangle newBounds, double margin, int steps)
    {
        setBounds(newBounds.getX(), newBounds.getY(), newBounds.getWidth(), newBounds.getHeight(), newBounds.getDiagonal()*margin, steps);
    }

    public void setTotalBounds(World world, double margin, boolean keepAspect, int steps)
    {
        Rectangle newBounds = newTotalBounds(world);

        if (newBounds != null) {
            double aspectRatio = bounds.getWidth() / bounds.getHeight();
    
            if (keepAspect) {
                newBounds.adjustAspectRatio(aspectRatio);
            }

            setBounds(newBounds, margin, steps);
        }
    }

    private Rectangle newTotalBounds(World world)
    {
        boolean first = true;

        Rectangle newBounds = null;

        for (CardNode node : world.getCardNodes()) {
            if (first) {
                newBounds = new Rectangle(node.getRectangle());
                first = false;
            }
            else {
                newBounds.union(node.getRectangle());
            }
        }

        return newBounds;
    }

    public void zoomBounds(double viewportX, double viewportY, double scale, int steps)
    {
        double newWidth  = scale * bounds.getWidth();
        double newHeight = scale * bounds.getHeight();

        double dw = bounds.getWidth()  - newWidth;
        double dh = bounds.getHeight() - newHeight;

        Rectangle zoomBounds = new Rectangle(bounds.getX() + dw / 2, bounds.getY() + dh / 2, newWidth, newHeight);

        Point viewportXY = new Point(viewportX, viewportY);
        Point originalXY = newWorldFromViewport(viewportXY, bounds);
        Point newXY      = newWorldFromViewport(viewportXY, zoomBounds);
        Point centerDXY  = newXY.newFromTo(originalXY);
        Rectangle newBounds =
            new Rectangle(zoomBounds.getX() + centerDXY.getX(),
                          zoomBounds.getY() + centerDXY.getY(),
                          zoomBounds.getWidth(),
                          zoomBounds.getHeight());

        initAnimation(newBounds, steps);
    }

    public void panBounds(double viewportDX, double viewportDY, int steps)
    {
        Rectangle newBounds =
            newPanBounds(viewportDX * getViewportToWorldRatioWidth(bounds), viewportDY *
                getViewportToWorldRatioHeight(bounds));

        initAnimation(newBounds, steps);
    }

    public void dragNode(CardNode cardNode, double viewportDX, double viewportDY)
    {
        NodeData nodeData = cardNode.getNode().getNodeData(); 
        nodeData.setX(nodeData.x() + (float)(viewportDX * getViewportToWorldRatioWidth(bounds)));
        nodeData.setY(nodeData.y() + (float)(viewportDY * getViewportToWorldRatioHeight(bounds)));
    }

    private Rectangle newPanBounds(double worldDX, double worldDY)
    {
        return new Rectangle(bounds.getX() + worldDX, bounds.getY() + worldDY, bounds.getWidth(), bounds.getHeight());
    }

    private double getViewportToWorldRatioWidth(Rectangle world)
    {
        return world.getWidth() / viewport.getWidth();
    }

    private double getViewportToWorldRatioHeight(Rectangle world)
    {
        return world.getHeight() / viewport.getHeight();
    }

    public void setFocusRectangle(CardNode cardNode, double smooth, double margin, int steps)
    {
        focusRectangle = cardNode.getRectangle();
        focusSmooth    = smooth;
        focusSteps     = steps;
        focusMargin    = margin;
        focusWaypoints = null; // setting a rectangle interrupts way-point path, but not visa versa
    }

    private boolean isAnimating;

    public boolean isAnimating()
    {
        return isAnimating;
    }

    public boolean stepAnimation()
    {
        isAnimating = false;

        if (focusSteps <= 0) {
            focusRectangle = null;
        }

        if (focusWaypoints != null && focusWaypoints.isEmpty()) {
            focusWaypoints = null;
        }

        if (focusWaypoints != null && focusRectangle == null) {
            FocusWaypoint focusWaypoint = focusWaypoints.poll();
            focusRectangle = focusWaypoint.getRectangle();
            focusSmooth    = focusWaypoint.getSmooth();
            focusMargin    = focusWaypoint.getMargin();
            focusSteps     = focusWaypoint.getSteps();
        }

        if (focusRectangle != null && focusSteps-- > 0) {
            bounds0.copy(bounds);
            double margin = focusRectangle.getDiagonal()*focusMargin;

            bounds1.setSize(focusRectangle.getWidth()+margin, focusRectangle.getHeight()+margin);
            bounds1.setPosition(focusRectangle.getX()-margin/2, focusRectangle.getY()-margin/2);

            bounds.setBetween(bounds0, bounds1, focusSmooth);

            isAnimating = true;

            return true;
        }
        else if (steps > 0 && t < 1) {
            bounds.setBetween(bounds0, bounds1, t);
            t += 1.0 / (double) steps;
            
            isAnimating = true;

            return true;
        }
        else {
            return false;
        }
    }

    private void initAnimation(Rectangle newBounds, int steps)
    {
        this.steps = steps;

        if (steps > 0) {
            t = 0;

            bounds0.copy(bounds);
            bounds1.copy(newBounds);
        }
        else {
            bounds.copy(newBounds);
        }

        // cancel any window following
        focusRectangle = null;
        focusWaypoints = null;
    }

    private Point newWorldFromViewport(Point viewportPoint, Rectangle world)
    {
        return
            new Point(
                transformViewportToWorldX(viewportPoint.getX(), world),
                transformViewportToWorldY(viewportPoint.getY(), world));
    }

    private double transformViewportToWorldX(double viewportX, Rectangle world)
    {
        return (viewportX - viewport.getX()) * getViewportToWorldRatioWidth(world) + world.getX();
    }

    private double transformViewportToWorldY(double viewportY, Rectangle world)
    {
        return (viewportY - viewport.getY()) * getViewportToWorldRatioHeight(world) + world.getY();
    }

    private Rectangle viewport;

    private Rectangle bounds;

    private Rectangle bounds0;
    private Rectangle bounds1;
    private double    t;
    private int       steps = 0;

    private Rectangle focusRectangle;

    private Queue<FocusWaypoint> focusWaypoints;

    public void setFocusWaypoints(Queue<FocusWaypoint> focusWaypoints)
    {
        this.focusWaypoints = focusWaypoints;
    }

    public void setBounds(CardNode cardNode, double margin, int steps)
    {
        setBounds(cardNode.getRectangle(), margin, steps);
    }

    public Rectangle getBounds()
    {
        return bounds;
    }
}
