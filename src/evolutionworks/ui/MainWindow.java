package evolutionworks.ui;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.time.StopWatch;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;

import de.matthiasmann.twl.DesktopArea;
import de.matthiasmann.twl.Event;
import de.matthiasmann.twl.GUI;
import de.matthiasmann.twl.Widget;
import de.matthiasmann.twl.renderer.MouseCursor;
import de.matthiasmann.twl.renderer.lwjgl.LWJGLRenderer;
import de.matthiasmann.twl.theme.ThemeManager;
import de.matthiasmann.twleffects.lwjgl.LWJGLEffectsRenderer;

import evolutionworks.layout.Animator;
import evolutionworks.layout.NetworkBuilder;
import evolutionworks.layout.Window;
import evolutionworks.layout.World;
import evolutionworks.layout.Rectangle;

public class MainWindow
{
    static final String          WITH_TITLE    = "resizableframe-title";
    static final String          WITHOUT_TITLE = "resizableframe";

    private static LWJGLRenderer renderer;
    private static GUI           gui;
    private static boolean       runAnimation  = true;

    private static TabbedBrowser tabbedBrowser = new TabbedBrowser();

    static final RootPane        ROOT          = new RootPane();

    private static boolean       isFullscreen  = false;
    private static boolean       doChangeMode  = false;

    private static int display_x = 1300, display_y = 700;
    
    public static void show()
    {
        Logger log = Logger.getLogger("evolutionworks.ui.MainWindow");

        try {
            createDisplay();
            mainLoop(false);
        }
        catch (LWJGLException e) {
            log.log(Level.SEVERE, "Uncaught LWJGL exception", e);
        }
        catch (IOException e) {
            log.log(Level.SEVERE, "Uncaught IO exception", e);
        }

        Display.destroy();
    }

    private static void createDisplay() throws LWJGLException
    {
        Display.setTitle("EvolutionWorks");
        
        if (isFullscreen) {
            Display.setFullscreen(true);
        }
        else {
            Display.setFullscreen(false);
            Display.setResizable(true);
            Display.setDisplayMode(new DisplayMode(display_x, display_y));
        }
        
        Display.create(new PixelFormat(0, 0, 0));
        
        Display.setVSyncEnabled(true);
        Display.setSwapInterval(1);

    }

    private static ThemeManager theme;

    private static void loadTheme() throws IOException
    {
        renderer.syncViewportSize();

        theme =
            ThemeManager.createThemeManager(MainWindow.class.getResource("evolutionworks.xml"), renderer);

        gui.setSize();
        gui.applyTheme(theme);
    }

    private static void mainLoop(boolean isApplet) throws LWJGLException, IOException
    {
        renderer = new LWJGLEffectsRenderer();

        renderer.setUseSWMouseCursors(false);
        renderer.setUseQuadsForLines(false);

        gui = new GUI(ROOT, renderer);

        loadTheme();

        if (evolutionworks.Browser.isTraditional()) {
            tabbedBrowser.setSize(ROOT.desk.getInnerWidth(), ROOT.desk.getInnerHeight());
            ROOT.desk.add(tabbedBrowser);
        }
        else {
            tabbedBrowser.setSize(400, ROOT.getInnerHeight());
            ROOT.add(tabbedBrowser);
        }

        tabbedBrowser.setPosition(0, 0);

        if (evolutionworks.Browser.isUserTest()) {
            tabbedBrowser.addTab("about:reading-list", null, false);
            tabbedBrowser.addTab("keywords:" + evolutionworks.Browser.getTestKeywords(), null, false);

            evolutionworks.Browser.startTest();
        }

        ROOT.layoutAnimator.initAnimation(ROOT.layoutWorld);

        while (!Display.isCloseRequested()) {
            if (doChangeMode) {
                doChangeMode = false;
                
                gui.destroy();
                renderer.getActiveCacheContext().destroy();
                Display.destroy();

                createDisplay();

                GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
                renderer.syncViewportSize();
                
                loadTheme();

                gui.clearKeyboardState();
                gui.clearMouseState();
            }
            else if (Display.wasResized()) {
                if (!Display.isFullscreen()) {
                    display_x = Display.getWidth();
                    display_y = Display.getHeight();
                }
                
                GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
                renderer.syncViewportSize();
            }

            boolean graph_animated;
            boolean window_animated;
            
            if (runAnimation) {
                graph_animated  = ROOT.layoutAnimator.stepAnimation();
                window_animated = ROOT.layoutWindow.stepAnimation();
            }
            else {
                graph_animated  = false;
                window_animated = false;
            }

            CardUI.setSimple();
            
            if (graph_animated || window_animated) {
                ROOT.invalidateLayout();
            }
            else if (!Display.isVisible() || !Display.isActive()) {
                Display.sync(5);
                
                gui.clearKeyboardState();
                gui.clearMouseState();
            }
          
            gui.update();
            Display.update();
            Display.sync(60);
            TestUtils.reduceInputLag();
        }
    }

    /**
     * @return the runAnimation
     */
    boolean isRunAnimation()
    {
        return runAnimation;
    }

    /**
     * @param runAnimation the runAnimation to set
     */
    static void setRunAnimation(boolean runAnimationIn)
    {
        runAnimation = runAnimationIn;
    }

    static class RootPane extends Widget
    {
        final DesktopArea desk;

        // evolutionworks layout - jwilkins
        NetworkBuilder    layoutNetworkBuilder;
        World             layoutWorld;
        Animator          layoutAnimator;
        Window            layoutWindow;

        public RootPane()
        {
            setTheme("");

            desk = new DesktopArea();
            desk.setTheme("");

            add(desk);

            Logger log = Logger.getLogger("evolutionworks.RootPane");

            try {
                layoutNetworkBuilder = evolutionworks.layout.Factory.getNetworkBuilder();
                layoutWorld          = layoutNetworkBuilder.getWorld();
                layoutAnimator       = new Animator();
                layoutWindow         = new Window();
            }
            catch (Exception e) {
                log.log(Level.SEVERE, "Graph layout initialization failure", e);
            }
        }

        private final float background_r = 1; //100.0f / 255.0f;
        private final float background_g = 1; //149.0f / 255.0f;
        private final float background_b = 1; //236.0f / 255.0f;

        private boolean selectZoomStarted = false;
        private int selectZoomX0;
        private int selectZoomX1;
        private int selectZoomY0;
        private int selectZoomY1;

        @Override
        protected void paintBackground(GUI gui)
        {
            GL11.glClearColor(background_r, background_g, background_b, 1.0f);
            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
        }

        private void renderSelect()
        {
           if (selectZoomStarted) {
               GL11.glLineWidth(0);

               GL11.glColor4f(1,1,1,1);
               GL11.glBlendFunc(GL11.GL_ONE_MINUS_DST_COLOR, GL11.GL_ZERO);
           
               GL11.glBegin(GL11.GL_LINE_LOOP);
               GL11.glVertex2d(selectZoomX0, selectZoomY0);
               GL11.glVertex2d(selectZoomX1, selectZoomY0);
               GL11.glVertex2d(selectZoomX1, selectZoomY1);
               GL11.glVertex2d(selectZoomX0, selectZoomY1);
               GL11.glEnd();
               
               GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
           }
        }

        private void renderGrid(Window window)
        {
           final double att = 0.25;
           final double r = att * background_r;
           final double g = att * background_g;
           final double b = att * background_b;

           Rectangle bounds = window.getBounds();

           double log10Width     = Math.log10(bounds.getWidth());
           double logFloorWidth  = Math.floor(log10Width);
           double worldGridWidth = Math.pow(10, logFloorWidth);

           double log10Height     = Math.log10(bounds.getHeight());
           double logFloorHeight  = Math.floor(log10Height);
           double worldGridHeight = Math.pow(10, logFloorHeight);

           double widgetHeight = getInnerHeight();
           double widgetWidth  = getInnerWidth();

           double worldLeft = Math.floor(bounds.getX() / worldGridWidth ) * worldGridWidth;
           double worldTop  = Math.floor(bounds.getY() / worldGridHeight) * worldGridHeight;

           double logFract = 1 - Math.sqrt(
                                     (log10Width  - logFloorWidth ) *
                                     (log10Height - logFloorHeight) /
                                     4);

           double widgetGridWidth;
           double widgetGridHeight;

           double widgetLeft;
           double widgetTop;

           GL11.glLineWidth(1);

           GL11.glBegin(GL11.GL_LINES);

           if (logFract < 0.5) {
               GL11.glColor3d(background_r, background_g, background_b);
           }
           else {
               double fade = 2*(logFract-0.5);

               GL11.glColor3d(
                   background_r + fade*(r-background_r),
                   background_g + fade*(g-background_g),
                   background_b + fade*(b-background_b));
           }

           widgetGridWidth  = window.getWorldToViewportRatioWidth () * worldGridWidth  / 10;
           widgetGridHeight = window.getWorldToViewportRatioHeight() * worldGridHeight / 10;

           widgetLeft = window.transformWorldToViewportX(worldLeft);
           widgetTop  = window.transformWorldToViewportY(worldTop);

           if (widgetGridWidth > 1) {
               while(widgetLeft < widgetWidth) {
                   GL11.glVertex2d(widgetLeft, 0);
                   GL11.glVertex2d(widgetLeft, widgetHeight);
                   widgetLeft += widgetGridWidth;
               }
           }

           if (widgetGridHeight > 1) {
               while(widgetTop < widgetHeight) {
                   GL11.glVertex2d(0,           widgetTop);
                   GL11.glVertex2d(widgetWidth, widgetTop);
                   widgetTop += widgetGridHeight;
               }
           }

           if (logFract < 0.5) {
               double fade = 2*logFract;

               GL11.glColor3d(
                   background_r + fade*(r-background_r),
                   background_g + fade*(g-background_g),
                   background_b + fade*(b-background_b));
           }
           else {
               GL11.glColor3d(r, g, b);
           }

           widgetGridWidth  = window.getWorldToViewportRatioWidth () * worldGridWidth ;
           widgetGridHeight = window.getWorldToViewportRatioHeight() * worldGridHeight;

           widgetLeft = window.transformWorldToViewportX(worldLeft);
           widgetTop  = window.transformWorldToViewportY(worldTop);

           if (widgetGridWidth >= 1) {
			   while(widgetLeft < widgetWidth) {
				   GL11.glVertex2d(widgetLeft, 0);
				   GL11.glVertex2d(widgetLeft, widgetHeight);
				   widgetLeft += widgetGridWidth;
			   }
           }

           if (widgetGridHeight >= 1) {
			   while(widgetTop < widgetHeight) {
				   GL11.glVertex2d(0,           widgetTop);
				   GL11.glVertex2d(widgetWidth, widgetTop);
				   widgetTop += widgetGridHeight;
			   }
           }

           GL11.glEnd();
        }
        
        @Override
        protected void paintWidget(GUI gui)
        {
            renderGrid(layoutWindow);
            layoutWindow.renderArrows(layoutWorld);       
        }

        @Override
        protected void paintDragOverlay(GUI gui, int mouseX, int mouseY, int modifier)
        {
            renderSelect();
        }
        
        @Override
        protected void layout()
        {
            if (gui == null)
                return;

            setPosition(gui.getInnerX(), gui.getInnerY());
            setSize(gui.getInnerWidth(), gui.getInnerHeight());
            
            desk.setPosition(getInnerX(), getInnerY());
            desk.setSize(getInnerWidth(), getInnerHeight());

            tabbedBrowser.setPosition(desk.getInnerX(), desk.getInnerY());

            if (evolutionworks.Browser.isTraditional()) {
                tabbedBrowser.setSize(desk.getInnerWidth(), desk.getInnerHeight());
            }
            else {
                tabbedBrowser.setSize(400, getInnerHeight());
            }

            layoutWindow.setViewport(
                desk.getInnerX() + tabbedBrowser.getWidth(),
                desk.getInnerY(),
                desk.getInnerWidth() - tabbedBrowser.getWidth(),
                desk.getInnerHeight());

            layoutWindow.positionWidgets(layoutWorld);
        }

        @Override
        protected void afterAddToGUI(GUI gui)
        {
            super.afterAddToGUI(gui);
            validateLayout();
        }

        @Override
        protected boolean handleEvent(Event evt)
        {
            if (evt.getType() == Event.Type.KEY_PRESSED &&
               (evt.getKeyCode() == Event.KEY_F11 || (evt.getKeyCode() == Event.KEY_F && (evt.getModifiers()&(Event.MODIFIER_META|Event.MODIFIER_SHIFT)) != 0)))
            {
                isFullscreen = !isFullscreen;
                doChangeMode = true;
            }
            
            if (!evt.isMouseDragEvent()) {
                if (evt.getType() == Event.Type.MOUSE_BTNDOWN && evt.getMouseButton() == Event.MOUSE_LBUTTON) {
                    dragIsStarted = true;
                    dragPosX = evt.getMouseX();
                    dragPosY = evt.getMouseY();
                    
                    return true;
                }
                else if (evt.getType() == Event.Type.MOUSE_BTNDOWN && evt.getMouseButton() == Event.MOUSE_MBUTTON) {
                    selectZoomStarted = true;
                    selectZoomX0 = selectZoomX1 = evt.getMouseX();
                    selectZoomY0 = selectZoomY1 = evt.getMouseY();
                    return true;
                }
                else if (evt.getType() == Event.Type.MOUSE_CLICKED && evt.getMouseButton() == Event.MOUSE_LBUTTON) {
                    int mouseClickCount = evt.getMouseClickCount();

                    if (mouseClickCount > 1) {
                        layoutWindow.setTotalBounds(layoutWorld, 0.1, true, 120);
                        invalidateLayout();                        
                    }
                }
            }

            if (dragIsStarted || selectZoomStarted) {
                if (evt.isMouseDragEnd()) {
                    dragIsStarted = false;
                    
                    if (selectZoomStarted) {
                       layoutWindow.setBoundsFromViewport(
                           selectZoomX0 < selectZoomX1 ? selectZoomX0 : selectZoomX1,
                           selectZoomY0 < selectZoomY1 ? selectZoomY0 : selectZoomY1,
                           Math.max(Math.abs(selectZoomX1 - selectZoomX0), 1),
                           Math.max(Math.abs(selectZoomY1 - selectZoomY0), 1),
                           0.1,
                           30);
                               
                       selectZoomStarted = false;
                    }
                }
                else if (evt.getType() == Event.Type.MOUSE_DRAGGED) {
                   if (dragIsStarted) {
                       int x = evt.getMouseX();
                       int y = evt.getMouseY();
                       layoutWindow.panBounds(-(x - dragPosX), -(y - dragPosY), 0);
                       invalidateLayout();
                       dragPosX = x;
                       dragPosY = y;
                   }
                   else if (selectZoomStarted) {
                       int x = evt.getMouseX();
                       int y = evt.getMouseY();
                       invalidateLayout();
                       selectZoomX1 = x;
                       selectZoomY1 = y;
                   }
                }

                return true;
            }

            if (evt.getType() == Event.Type.MOUSE_WHEEL) {
                int x = evt.getMouseX();
                int y = evt.getMouseY();
                layoutWindow.zoomBounds(x, y, 1 + 0.1 * evt.getMouseWheelDelta(), 0);
                invalidateLayout();
                
                return true;
            }

            return super.handleEvent(evt);
        }

        private boolean dragIsStarted = false;
        private int     dragPosX, dragPosY;
    }

    static String    queryName;
    static StopWatch queryStopWatch = new StopWatch();

    public static void startWaitCursor(String url)
    {
        queryName = url;
        queryStopWatch.reset();
        queryStopWatch.start();
        MouseCursor cursor = (MouseCursor) theme.getCursor("cursor.wait");
        renderer.setCursor(cursor);
    }

    public static void endWaitCursor()
    {
        evolutionworks.Browser.logTimer(queryName, queryStopWatch.getTime());
        renderer.setCursor(null);
    }
    
    private MainWindow()
    {}
}
