package evolutionworks.ui;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.matthiasmann.twl.Container;
import de.matthiasmann.twl.ScrollPane;
import de.matthiasmann.twl.TextArea;
import de.matthiasmann.twl.textarea.HTMLTextAreaModel;
import de.matthiasmann.twl.textarea.StyleSheet;

public class HTMLPane extends Container
{
    private static final class LinkCallbackHandler implements TextArea.Callback
    {
        private final Navigator parent;
        private final String    myHref;

        private LinkCallbackHandler(Navigator parent, String myHref)
        {
            this.parent = parent;
            this.myHref = myHref;
        }

        private static final String COMMAND_PREFIX = "command:";

        public void handleLinkClicked(String linkHref)
        {
            if (linkHref.startsWith(COMMAND_PREFIX)) {
                String command = linkHref.substring(COMMAND_PREFIX.length());

                if (command.equals("close")) {
                    parent.close(myHref);
                }
                else if (command.equals("add-to-reading-list")) {
                    parent.addToReadingList(myHref, false);
                }
                else if (command.equals("remove-from-reading-list")) {
                    parent.removeFromReadingList(myHref, false);
                }
                else if (command.equals("zoom-out")) {
                    CardUI.zoomOut(myHref);
                }
                else if (command.equals("submit-reading-list")) {
                    evolutionworks.Browser.endTest();
                    parent.showReadingList(false);
                }
            }
            else {
                parent.navigateTo(linkHref, myHref, true);
            }
        }
    }

    private final HTMLTextAreaModel textAreaModel;
    private final TextArea          textArea;
    private final ScrollPane        scrollPane;

    HTMLPane(final Navigator parent, final String myHref)
    {
        textAreaModel = new HTMLTextAreaModel();

        textArea = new TextArea(textAreaModel);
        textArea.addCallback(new LinkCallbackHandler(parent, myHref));

        scrollPane = new ScrollPane(textArea);
        scrollPane.setFixed(ScrollPane.Fixed.HORIZONTAL);
        scrollPane.setExpandContentSize(true);

        add(scrollPane);
    }

    @Override
    protected void layout()
    {
        scrollPane.setSize(getInnerWidth(), getInnerHeight());
        scrollPane.setPosition(getInnerX(), getInnerY());
    }

    /* (non-Javadoc)
     * @see de.matthiasmann.twl.DialogLayout#getPreferredInnerHeight()
     * A bit of a hack to make sure the tab fills as much space as possible
     */
    @Override
    public int getPreferredInnerHeight()
    {
        return MainWindow.ROOT.desk.getHeight();
    }

    /* (non-Javadoc)
     * A bit of a hack to make sure the tab fills as much space as possible
     * @see de.matthiasmann.twl.DialogLayout#getPreferredInnerWidth()
     */
    @Override
    public int getPreferredInnerWidth()
    {
        return MainWindow.ROOT.desk.getWidth();
    }

    private void setStylesheet()
    {
        StyleSheet styleSheet = new StyleSheet();
        for (String styleSheetLink : textAreaModel.getStyleSheetLinks()) {
            try {
                styleSheet.parse(HTMLPane.class.getResource(styleSheetLink));
            }
            catch (IOException e) {
                Logger.getLogger(HTMLPane.class.getName()).log(
                    Level.WARNING,
                    "An exception occured while parsing HTML stylesheet: " + styleSheetLink,
                    e);
            }
        }

        textArea.setStyleClassResolver(styleSheet);
    }

    public void setHtml(String html)
    {
        textAreaModel.setHtml(html);
        setStylesheet();
    }
}
