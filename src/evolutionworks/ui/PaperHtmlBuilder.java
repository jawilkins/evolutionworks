package evolutionworks.ui;

import org.apache.commons.lang3.StringEscapeUtils;

import evolutionworks.model.ModelPaper;
import evolutionworks.model.ModelPaperList;

public class PaperHtmlBuilder
{
    private StringBuilder body = new StringBuilder();

    public void clear()
    {
        body.setLength(0);
    }

    private String escape(String text)
    {
        return StringEscapeUtils.escapeXml10(text);
    }

    private StringBuilder elem(String typeName, String className, Object content)
    {
        StringBuilder sb = new StringBuilder();

        sb.append("<").append(typeName);
        if (className != null) {
            sb.append(" class=\"").append(className).append("\"");
        }
        sb.append(">");

        sb.append(content);

        sb.append("</").append(typeName).append(">");

        return sb;
    }

    private StringBuilder elem(String elemName, Object content)
    {
        return elem(elemName, null, content);
    }

    private StringBuilder link(String type, String rel, String href)
    {
        StringBuilder sb = new StringBuilder();

        sb.append("<link");
        sb.append(" type=\"").append(type).append("\"");
        sb.append(" rel=\"").append(rel).append("\"");
        sb.append(" href=\"").append(href).append("\"");
        sb.append("/>");

        return sb;
    }

    private String makeTitle(ModelPaper modelPaper, String elemName, String className)
    {
        String title = escape(modelPaper.getTitle());

        if (title == null) {
            title = "Title Unavailable";
        }

        return elem(elemName, className, title).toString();
    }

    public void writeTitle(ModelPaper modelPaper)
    {
        body.append(makeTitle(modelPaper, "h1", "main-title"));
    }

    private String makeAuthors(ModelPaper modelPaper, String elemName, String className)
    {
        StringBuilder sb = new StringBuilder();

        int authorCount = modelPaper.getAuthorCount();

        if (authorCount > 0) {
            sb.append(modelPaper.getAuthor(0));
            
            for (int i = 1; i < authorCount; i++) {
                if (authorCount != 2) {
                    sb.append(", ");
                }
                else {
                    sb.append(' ');
                }
                
                if (i == authorCount-1) {
                    sb.append("and ");
                }
                
                sb.append(escape(modelPaper.getAuthor(i)));
            }
        }

        return elem(elemName, className, sb).toString();
    }

    public void writeAuthors(ModelPaper modelPaper)
    {
        body.append(makeAuthors(modelPaper, "div", "main-authors"));
    }

    public void writeAbstract(ModelPaper modelPaper)
    {
        String theAbstract = modelPaper.getAbstract();
        
        if (theAbstract != null) {
            body.append(elem("h2", "header-abstract", "Abstract"));
            body.append(elem("p", "main-abstract", escape(theAbstract)));
        }
    }

    public void writeCitationCount(ModelPaper modelPaper)
    {
        int citationCount = modelPaper.getCitationCount();
        
        if (citationCount > 0) {
            body.append(elem("div", "main-citation-count", "Cited by " + citationCount));
        }
    }

    public void writeReferences(ModelPaper modelPaper)
    {
        int referenceCount = modelPaper.getReferenceCount();
        
        if (referenceCount > 0) {
            body.append(elem("h2", "header-list", "References (" + referenceCount + ")"));
            writePaperList(modelPaper.getReferences());
        }
    }

    public void writeCitations(ModelPaper modelPaper)
    {
        int citationCount = modelPaper.getCitationCount();

        if (citationCount > 0) {
            body.append(elem("h2", "header-list", "Citations (" + citationCount + ")"));
            writePaperList(modelPaper.getCitations());
        }
    }

    public void writePaperList(ModelPaperList modelPaperList)
    {
        body.append(makePaperList(modelPaperList, "ul", "list-papers", "li", "item-paper"));
    }

    private String makePaperList(ModelPaperList modelPaperList, String elemList, String classNameList, String elemItem, String classNameItem)
    {
        StringBuilder sb = new StringBuilder();
        
        for (ModelPaper modelPaper : modelPaperList) {
            sb.append(makePaperItem(modelPaper, elemItem, classNameItem));
        }
        
        return elem(elemList, classNameList, sb).toString();
    }

    private String makePaperItem(ModelPaper modelPaper, String elemItem, String className)
    {
        String location = escape(modelPaper.getLocation());
        int authorCount = modelPaper.getAuthorCount();
        int citationCount = modelPaper.getCitationCount();

        StringBuilder sb = new StringBuilder();

        String title = makeTitle(modelPaper, "h3", "item-title");

        if (location != null) {
            sb.append(makeAnchor("item-title-anchor", location, title));
        }
        else {
            sb.append(title);
        }

        if (authorCount > 0) {
            sb.append(makeAuthors(modelPaper, "div", "item-authors"));
        }

        if (citationCount > 0) {
            sb.append(elem("div", "item-citation-count", "Cited by " + citationCount));
        }

        return elem(elemItem, className, sb).toString();
    }

    String makeAnchor(String className, String href, String content)
    {
        StringBuilder sb = new StringBuilder();

        sb.append("<a");

        if (className != null) {
            sb.append(" class=\"");
            sb.append(className);
            sb.append("\"");
        }

        sb.append(" href=\"");
        sb.append(href);
        sb.append("\">");
        sb.append(content);
        sb.append("</a>");

        return sb.toString();
    }

    void writeCommand(String command, String text)
    {
        body.append(makeAnchor("command", "command:" + escape(command), escape(text)));
    }

    public String getPaper(String title)
    {
        StringBuilder html = new StringBuilder(
            elem("html",
                elem("head",
                    elem("title", escape(title))
                    .append(link("text/css", "stylesheet", "evolutionworks.css")))
                .append(elem("body", body))));

        return html.toString();
        
        //StringBuilder html2 = new StringBuilder(
        //    elem("html",
        //        elem("head",
        //            elem("title", escape(title))
        //            .append(link("text/css", "stylesheet", "demo.css")))
        //        .append(elem("body", escape(html.toString())))));
        
        //return html2.toString();
        
    }

    public void writeElem(String elemName, String className, String content)
    {
        body.append(elem(elemName, className, content));
    }
}
