package evolutionworks.ui;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.DialogLayout;
import de.matthiasmann.twl.EditField;
import de.matthiasmann.twl.Event;
import de.matthiasmann.twl.TabbedPane;
import de.matthiasmann.twl.TabbedPane.Tab;

import evolutionworks.model.ModelPaper;
import evolutionworks.model.ModelPaperLinkedList;
import evolutionworks.model.ModelPaperList;
import evolutionworks.model.ModelQuery;

public class TabbedBrowser extends DialogLayout implements Navigator
{
    private EditField                  txtKeywords;
    private Button                     btnSearch;
    private final TabbedPane           tabbedPane;

    private final Map<String, HtmlTab> htmlTabs    = new HashMap<String, HtmlTab>();

    private ModelPaperLinkedList       readingList = new ModelPaperLinkedList();

    private void search()
    {
        if (txtKeywords.getTextLength() > 0) {
            addTab("keywords:" + txtKeywords.getText(), null, true);
        }
    }

    TabbedBrowser()
    {
        tabbedPane  = new TabbedPane();
        tabbedPane.setScrollTabs(true);

        btnSearch = new Button("Search");
        btnSearch.setEnabled(false);
        btnSearch.addCallback(new Runnable() {
            public void run()
            {
                search();
            }
        });

        txtKeywords = new EditField();
        txtKeywords.setMultiLine(false);
        txtKeywords.addCallback(new EditField.Callback() {
            public void callback(int key)
            {
                btnSearch.setEnabled(txtKeywords.getTextLength() > 0);
            }
        });

        if (evolutionworks.Browser.isUserTest()) {
            txtKeywords.setVisible(false);
            btnSearch.setVisible(false);
        }
        
        Group vSearchGroup = createParallelGroup(txtKeywords, btnSearch);
        Group hSearchGroup = createSequentialGroup(txtKeywords, btnSearch);

        Group vTabGroup = createParallelGroup(tabbedPane);
        Group hTabGroup = createParallelGroup(tabbedPane);

        setVerticalGroup(createSequentialGroup(vSearchGroup, vTabGroup));
        setHorizontalGroup(createParallelGroup(hSearchGroup, hTabGroup));

        setIncludeInvisibleWidgets(false);
    }

    @Override
    protected boolean handleEvent(Event evt)
    {
        if (txtKeywords.hasKeyboardFocus() &&
            evt.getType() == Event.Type.KEY_PRESSED &&
            evt.getKeyCode() == Event.KEY_RETURN)
        {
            search();
        }

        return super.handleEvent(evt);
    }

    private static final String KEYWORDS_PREFIX = "keywords:";

    void addTab(String href, String referer, boolean addCloseCommand)
    {
        evolutionworks.Browser.logEvent("addTab-href", href);
        evolutionworks.Browser.logEvent("addTab-referer", referer);

        if (href.equals(READING_LIST_HREF)) {
            showReadingList(addCloseCommand);
        }
        else if (href.startsWith(ModelQuery.HTTP_PREFIX) || href.startsWith(ModelQuery.TEST_PREFIX)) {
            addPaper(href, referer, addCloseCommand);
        }
        else if (href.startsWith(KEYWORDS_PREFIX)) {
            addPaperList(href, href.substring(KEYWORDS_PREFIX.length()).split("\\s+"), addCloseCommand);
        }
        else {
            final String className = TabbedBrowser.class.getName();
            IllegalArgumentException e = new IllegalArgumentException("unknown href: " + href);
            Logger.getLogger(className).throwing(className, "addTab", e);
            throw e;
        }
    }

    static final String READING_LIST_HREF  = "about:reading-list";
    static final String READING_LIST_TITLE = "Reading List";

    @Override
    public void showReadingList(boolean addCloseCommand)
    {
        evolutionworks.Browser.logEvent("showReadingList");

        final PaperHtmlBuilder paperHtmlBuilder = new PaperHtmlBuilder();

        //if (evolutionworks.Browser.isTraditional()) {
            if (addCloseCommand) {
                paperHtmlBuilder.writeCommand("close", "[Close Tab]");
            }
        //}

        paperHtmlBuilder.writeElem("h1", "main-title", READING_LIST_TITLE);

        if (!evolutionworks.Browser.isTraditional()) {
            paperHtmlBuilder.writeCommand("zoom-out", "[View All]");
        }

        if (readingList.size() == 0) {
            paperHtmlBuilder.writeElem("p", "info", "No papers have been added to the reading list.");
        }

        paperHtmlBuilder.writePaperList(readingList);

        if (evolutionworks.Browser.isUserTest()) {
            if (!evolutionworks.Browser.isTestCompleted()) {
                int needed = evolutionworks.Browser.getReadingListCount();
                int current = readingList.size();

                if (current == needed) {
                    paperHtmlBuilder.writeCommand("submit-reading-list", "[Submit this as the final reading list.]");
                }
                else {
                    paperHtmlBuilder.writeElem("p", "info", "Need to find " + Integer.toString(needed - current) + " more papers.");
                }
            }
            else {
                paperHtmlBuilder.writeElem("p", "info", "Thank You!");
            }
        }

        HtmlTab htmlTab = showExistingTab(READING_LIST_HREF);

        HTMLPane readingListPane;

        if (htmlTab == null) {
            readingListPane = new HTMLPane(this, READING_LIST_HREF);
            addHtmlTab(READING_LIST_HREF, READING_LIST_TITLE, readingListPane);
        }
        else {
            readingListPane = htmlTab.getHtmlPane();
        }

        readingListPane.setHtml(paperHtmlBuilder.getPaper(READING_LIST_TITLE));
    }

    void removeTab(String href)
    {
        evolutionworks.Browser.logEvent("removeTab-href", href);

        HtmlTab htmlTab = htmlTabs.get(href);

        if (htmlTab != null && htmlTab.getTab() != null) {
            tabbedPane.removeTab(htmlTab.getTab());
            htmlTab.setTab(null);
        }
    }

    void addPaper(String href, String referer, boolean addCloseCommand)
    {
        evolutionworks.Browser.logEvent("addPaper-href", href);
        evolutionworks.Browser.logEvent("addPaper-referer", referer);

        if (showExistingTab(href) != null || CardUI.showExistingCard(href, referer)) {
            return;
        }

        MainWindow.startWaitCursor(href);
        final ModelPaper modelPaper = ModelQuery.query(href);
        MainWindow.endWaitCursor();

        if (modelPaper != null) {
            final PaperHtmlBuilder paperHtmlBuilder = new PaperHtmlBuilder();

            //if (evolutionworks.Browser.isTraditional()) {
                if (addCloseCommand) {
                    paperHtmlBuilder.writeCommand("close", "[Close Tab]");
                }
            //}

            paperHtmlBuilder.writeTitle(modelPaper);
            paperHtmlBuilder.writeAuthors(modelPaper);
            paperHtmlBuilder.writeAbstract(modelPaper);
            paperHtmlBuilder.writeCitationCount(modelPaper);
            
            //if (!readingList.contains(modelPaper)) {
                paperHtmlBuilder.writeCommand("add-to-reading-list", "[Add to Reading List]");
            //}
            //else {
                paperHtmlBuilder.writeCommand("remove-from-reading-list", "[Remove from Reading List]");
            //}

            paperHtmlBuilder.writeReferences(modelPaper);
            paperHtmlBuilder.writeCitations(modelPaper);

            final String title = modelPaper.getTitle();

            final HTMLPane htmlPane = new HTMLPane(this, href);
            htmlPane.setHtml(paperHtmlBuilder.getPaper(title));

            if (evolutionworks.Browser.isTraditional()) {
                addHtmlTab(href, title, htmlPane);
            }
            else {
                addHtmlCard(href, referer, modelPaper);
            }
        }
    }

    void addPaperList(String href, String[] keywords, boolean addCloseCommand)
    {
        evolutionworks.Browser.logEvent("appPaperList-href", href);

        if (showExistingTab(href) != null) {
            return;
        }

        MainWindow.startWaitCursor(href);
        final ModelPaperList modelPaperList = ModelQuery.query(keywords);
        MainWindow.endWaitCursor();

        if (modelPaperList != null) {
            final PaperHtmlBuilder paperHtmlBuilder = new PaperHtmlBuilder();

            //if (evolutionworks.Browser.isTraditional()) {
                if (addCloseCommand) {
                    paperHtmlBuilder.writeCommand("close", "[Close Tab]");
                }
            //}

            if (!evolutionworks.Browser.isTraditional()) {
                paperHtmlBuilder.writeCommand("zoom-out", "[Zoom All]");
            }

            final String title = StringUtils.join(keywords, " ");

            paperHtmlBuilder.writeElem("h1", "main-title", "Search Results: " + title);
            paperHtmlBuilder.writePaperList(modelPaperList);

            final HTMLPane htmlPane = new HTMLPane(this, href);
            final String html = paperHtmlBuilder.getPaper(title);
            htmlPane.setHtml(html);

            addHtmlTab(href, title, htmlPane);
        }
    }

    private void addHtmlTab(String href, String title, HTMLPane htmlPane)
    {
        evolutionworks.Browser.logEvent("addHtmlTab-href", href);
        evolutionworks.Browser.logEvent("addHtmlTab-title", title);

        final String tabTitle;

        if (title.length() > 23) {
            tabTitle = title.substring(0, 20) + "...";
        }
        else {
            tabTitle = title;
        }

        Tab tab = tabbedPane.addTab(tabTitle, htmlPane);
        tabbedPane.setActiveTab(tab);

        HtmlTab htmlTab = new HtmlTab(tab, title, htmlPane);

        htmlTabs.put(href, htmlTab);
    }

    private void addHtmlCard(String href, String referer, ModelPaper modelPaper)
    {
        evolutionworks.Browser.logEvent("addHtmlTab-href", href);
        evolutionworks.Browser.logEvent("addHtmlTab-referer", referer);

        CardUI.insertCardUI(this, MainWindow.ROOT.desk, href, modelPaper, referer);
    }

    private HtmlTab showExistingTab(String href)
    {
        if (htmlTabs.containsKey(href)) {
            HtmlTab htmlTab = htmlTabs.get(href);

            if (htmlTab.getTab() == null) {
                final String title = htmlTab.getTitle();

                final String tabTitle;

                if (title.length() > 23) {
                    tabTitle = title.substring(0, 20) + "...";
                }
                else {
                    tabTitle = title;
                }

                htmlTab.setTab(tabbedPane.addTab(tabTitle, htmlTab.getHtmlPane()));
            }

            tabbedPane.setActiveTab(htmlTab.getTab());

            return htmlTab;
        }
        else {
            return null;
        }
    }

    private static class HtmlTab
    {
        private Tab            tab;
        private final String   title;
        private final HTMLPane htmlPane;

        HtmlTab(Tab tab, String title, HTMLPane htmlPane)
        {
            this.tab = tab;
            this.title = title;
            this.htmlPane = htmlPane;
        }

        /**
         * @return the tab
         */
        Tab getTab()
        {
            return tab;
        }

        /**
         * @param tab the tab to set
         */
        void setTab(Tab tab)
        {
            this.tab = tab;
        }

        /**
         * @return the title
         */
        String getTitle()
        {
            return title;
        }

        /**
         * @return the htmlPane
         */
        HTMLPane getHtmlPane()
        {
            return htmlPane;
        }
    }

    @Override
    public void navigateTo(String href, String referer, boolean addCloseCommand)
    {
        evolutionworks.Browser.logEvent("navigateTo-href", href);
        evolutionworks.Browser.logEvent("navigateTo-referer", referer);
        addTab(href, referer, addCloseCommand);
    }

    @Override
    public void close(String href)
    {
        evolutionworks.Browser.logEvent("close", href);
        removeTab(href);
        CardUI.removeCard(href);
    }

    @Override
    public void addToReadingList(String href, boolean addCloseCommand)
    {
        MainWindow.startWaitCursor(href);
        ModelPaper modelPaper = ModelQuery.query(href);
        MainWindow.endWaitCursor();

        if (!readingList.contains(modelPaper)) {
            readingList.add(modelPaper);
        }

        showReadingList(addCloseCommand);
        
        evolutionworks.Browser.logEvent("addToReadingList", href);
        logReadingList();
    }

    @Override
    public void removeFromReadingList(String href, boolean addCloseCommand)
    {
        readingList.remove(href);
        showReadingList(addCloseCommand);
        
        evolutionworks.Browser.logEvent("removeFromReadingList", href);
        logReadingList();
    }

    private void logReadingList()
    {
        Collection<String> rl = new LinkedList<String>();
        
        for (ModelPaper modelPaper : readingList) {
            rl.add(modelPaper.getLocation());
        }

        evolutionworks.Browser.changeReadingListCount(rl);
    }
}
