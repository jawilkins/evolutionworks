package evolutionworks.ui;

import org.lwjgl.opengl.GL11;

import evolutionworks.model.ModelArrowRenderer;

class Arrow implements ModelArrowRenderer
{
    private static boolean      initialized      = false;

    private static Arrow        theArrowRenderer = null;

    private static final double BASE_WIDTH       = 3;

    public static Arrow getArrowRenderer()
    {
        if (!initialized) {
            theArrowRenderer = new Arrow();
            initialized = true;
        }

        return theArrowRenderer;
    }

    public void render(double x0, double y0, double x1, double y1, double width)
    {
        double rx0, ry0;
        double rx1, ry1;
        double rx2, ry2;
        double rx3, ry3;

        double nx = -(y1-y0);
        double ny =  (x1-x0);

        double len = Math.sqrt(nx*nx + ny*ny);
        nx /= len;
        ny /= len;

        width *= baseWidth;

        nx *= width;
        ny *= width;

        rx0 = x0 + nx;
        ry0 = y0 + ny;

        rx1 = x0 - nx;
        ry1 = y0 - ny;

        rx2 = x1 - nx;
        ry2 = y1 - ny;

        rx3 = x1 + nx;
        ry3 = y1 + ny;

        GL11.glBegin(GL11.GL_QUADS);
        GL11.glColor3d(0.75, 0.75, 0.75);
        GL11.glVertex2d(rx0, ry0);
        GL11.glColor3d(0.75, 0.75, 0.75);
        GL11.glVertex2d(rx1, ry1);
        GL11.glColor3d(0.75, 0.75, 0.75);
        GL11.glVertex2d(rx2, ry2);
        GL11.glColor3d(0.75, 0.75, 0.75);
        GL11.glVertex2d(rx3, ry3);
        GL11.glEnd();
    }

    private static double baseWidth = BASE_WIDTH;
}
