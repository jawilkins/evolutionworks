package evolutionworks.ui;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import de.matthiasmann.twl.CallbackWithReason;
import de.matthiasmann.twl.Event;
import de.matthiasmann.twl.Label;
import de.matthiasmann.twl.Label.CallbackReason;
import de.matthiasmann.twl.ResizableFrame;
import de.matthiasmann.twl.Widget;

import evolutionworks.layout.CardNode;
import evolutionworks.layout.FocusWaypoint;
import evolutionworks.layout.LayoutUtils;

import evolutionworks.model.ModelCard;
import evolutionworks.model.ModelPaper;

class CardUI extends ResizableFrame implements ModelCard
{
    private final CardNode                   cardNode;

    private final ModelPaper                 modelPaper;

    private final HTMLPane                   htmlPane;
    private final Widget                     parentWidget;

    private final Label                      lblFloatingTitle     = new Label();

    private final String href;

    private boolean                          dragIsStarted;
    private int                              dragPosX;
    private int                              dragPosY;

    private static final Map<String, CardUI> htmlCardUIs          = new HashMap<String, CardUI>();

    private static final double              CITATION_SIZE_FACTOR = 0.001;
    private static final int                 MAX_CARD_SIZE        = 1000;
    private static final int                 MIN_CARD_SIZE        = 10;

    private static int calcSize(ModelPaper resultModel)
    {
        return (int) ((MAX_CARD_SIZE - MIN_CARD_SIZE) *
            (CITATION_SIZE_FACTOR * Math.max(resultModel.getCitationCount(), 1.0)) + MIN_CARD_SIZE);
    }

    static void insertCardUI(Navigator navigator, Widget parentWidget, String href, ModelPaper modelPaper, String parentKey)
    {
        assert modelPaper != null;

        final String location = modelPaper.getLocation();

        CardUI cardUI;

        if (htmlCardUIs.containsKey(href)) {
            cardUI = htmlCardUIs.get(href);
        }
        else if (htmlCardUIs.containsKey(location)) {
            cardUI = htmlCardUIs.get(location);
        }
        else {
            cardUI = new CardUI(navigator, parentWidget, href, modelPaper, parentKey);

            htmlCardUIs.put(href, cardUI);

            if (!href.equals(location)) {
                htmlCardUIs.put(location, cardUI);
            }

            parentWidget.insertChild(cardUI, 0);

            MainWindow.ROOT.layoutAnimator.primeAnimation(120);
        }

        assert cardUI != null;

        CardUI parentCardUI = htmlCardUIs.get(parentKey);

        if (parentCardUI != null) {
            Queue<FocusWaypoint> focusWaypoints = new LinkedList<FocusWaypoint>();
            focusWaypoints.add(new FocusWaypoint(cardUI.cardNode, 0.05, 2.00, 100));
            focusWaypoints.add(new FocusWaypoint(cardUI.cardNode, 0.05, 0.05, 100));
            MainWindow.ROOT.layoutWindow.setFocusWaypoints(focusWaypoints);
        }
        else {
            MainWindow.ROOT.layoutWindow.setFocusRectangle(cardUI.cardNode, 0.1, 0.05, 120);
        }
    }

    private CardUI(Navigator navigator, Widget parentWidget, final String href, ModelPaper modelPaper, String parentKey)
    {
        assert modelPaper != null;

        this.href = href;
        this.parentWidget = parentWidget;
        this.modelPaper = modelPaper;

        final String title = modelPaper.getTitle();

        setTitle(title != null ? title : "Title Unavailable");

        lblFloatingTitle.setText(title);
        add(lblFloatingTitle);

        setTooltipContent(title);

        int size = calcSize(modelPaper);

        htmlPane = initHtmlPane(navigator, modelPaper, size);

        Collection<String> links = modelPaper.getLinks();

        if (parentKey != null && !links.contains(parentKey)) {
            links.add(parentKey);
        }

        double windowWidth = MainWindow.ROOT.desk.getWidth();
        double windowHeight = MainWindow.ROOT.desk.getHeight();
        double windowX = -windowWidth / 2;
        double windowY = -windowHeight / 2;

        cardNode =
            LayoutUtils.newCardNode(
                MainWindow.ROOT.layoutNetworkBuilder,
                modelPaper.getLocation(),
                links,
                this,
                Arrow.getArrowRenderer(),
                parentKey,
                windowX,
                windowY,
                windowWidth,
                windowHeight,
                size,
                size);

        lblFloatingTitle.addCallback(new CallbackWithReason<CallbackReason>(){

            @Override
            public void callback(CallbackReason reason)
            {
                if (reason == CallbackReason.CLICK || reason == CallbackReason.DOUBLE_CLICK) {
                    MainWindow.ROOT.layoutWindow.setFocusRectangle(cardNode, 0.1, 0.05, 60);
                }
            }
        });
    }

    private HTMLPane initHtmlPane(Navigator navigator, ModelPaper modelPaper, int size)
    {
        HTMLPane htmlPane = new HTMLPane(navigator, modelPaper.getLocation());
        htmlPane.setPosition(0, 0);
        htmlPane.setSize(size, size);
        add(htmlPane);

        if (modelPaper != null) {
            final PaperHtmlBuilder paperHtmlBuilder = new PaperHtmlBuilder();

            paperHtmlBuilder.writeCommand("zoom-out", "[Zoom Out]");

            paperHtmlBuilder.writeTitle(modelPaper);
            paperHtmlBuilder.writeAuthors(modelPaper);
            paperHtmlBuilder.writeAbstract(modelPaper);
            paperHtmlBuilder.writeCitationCount(modelPaper);
            paperHtmlBuilder.writeCommand("add-to-reading-list", "[Add to Reading List]");
            paperHtmlBuilder.writeCommand("remove-from-reading-list", "[Remove from Reading List]");
            paperHtmlBuilder.writeReferences(modelPaper);
            paperHtmlBuilder.writeCitations(modelPaper);

            final String title = modelPaper.getTitle();

            htmlPane.setHtml(paperHtmlBuilder.getPaper(title));
        }

        return htmlPane;
    }

    static void removeCard(String href)
    {
        CardUI cardUI = htmlCardUIs.get(href);

        if (cardUI != null) {
            MainWindow.ROOT.layoutNetworkBuilder.removeNode(cardUI.cardNode);
        }
    }

    static void zoomOut(String href)
    {
        CardUI cardUI = htmlCardUIs.get(href);

        if (cardUI != null) {
            MainWindow.ROOT.layoutWindow.setFocusRectangle(cardUI.cardNode, 0.1, 3.0, 120);
        }
        else {
            MainWindow.ROOT.layoutWindow.setTotalBounds(MainWindow.ROOT.layoutWorld, 0.1, true, 120);
        }
    }

    static boolean showExistingCard(String href, String referer)
    {
        CardUI parentCardUI = htmlCardUIs.get(referer);
        CardUI cardUI = htmlCardUIs.get(href);

        if (cardUI != null) {
            if (parentCardUI != null) {
                Queue<FocusWaypoint> focusWaypoints = new LinkedList<FocusWaypoint>();
                focusWaypoints.add(new FocusWaypoint(cardUI.cardNode, 0.05, 2.00, 100));
                focusWaypoints.add(new FocusWaypoint(cardUI.cardNode, 0.05, 0.05, 100));
                MainWindow.ROOT.layoutWindow.setFocusWaypoints(focusWaypoints);
            }
            else {
                MainWindow.ROOT.layoutWindow.setFocusRectangle(cardUI.cardNode, 0.1, 0.05, 120);                
            }
        }

        return cardUI != null;
    }

    @Override
    public void close()
    {
        parentWidget.removeChild(this);
        MainWindow.ROOT.layoutAnimator.primeAnimation(120);

        final String location = modelPaper.getLocation();

        htmlCardUIs.remove(href);

        if (!href.equals(location)) {
            htmlCardUIs.remove(location);
        }
        
        this.destroy();

        MainWindow.ROOT.layoutWindow.setTotalBounds(MainWindow.ROOT.layoutWorld, 0.1, true, 120);
    }

    @Override
    protected boolean handleEvent(Event evt)
    {
        if (!evt.isMouseDragEvent() &&
            evt.getType() == Event.Type.MOUSE_CLICKED &&
            evt.getMouseButton() == Event.MOUSE_LBUTTON)
        {
//            int mouseClickCount = evt.getMouseClickCount();

//            if (mouseClickCount == 1) {
                MainWindow.ROOT.layoutWindow.setFocusRectangle(cardNode, 0.1, 0.05, 60);
//            }
//            else {
//                MainWindow.ROOT.layoutWindow.setFocusRectangle(cardNode, 0.1, 3.00, 60);
//            }
        }

        if (!evt.isMouseDragEvent() &&
            (evt.getType() == Event.Type.MOUSE_BTNDOWN && evt.getMouseButton() == Event.MOUSE_LBUTTON)) {
            dragIsStarted = true;
            //MainWindow.setRunAnimation(false);
            MainWindow.ROOT.layoutAnimator.enableCollisions(false);
            dragPosX = evt.getMouseX();
            dragPosY = evt.getMouseY();

            return true;
        }

        if (dragIsStarted) {
            if (evt.isMouseDragEnd()) {
                dragIsStarted = false;
                MainWindow.ROOT.layoutAnimator.enableCollisions(true);
                //MainWindow.setRunAnimation(true);
            }
            else if (evt.getType() == Event.Type.MOUSE_DRAGGED) {
                int x = evt.getMouseX();
                int y = evt.getMouseY();
                MainWindow.ROOT.layoutWindow.dragNode(cardNode, (double) ((x - dragPosX)), (double) ((y - dragPosY)));
                MainWindow.ROOT.layoutAnimator.primeAnimation(120);
                dragPosX = x;
                dragPosY = y;
            }

            return true;
        }

        return true;
    }

    private static final int MIN_WIDTH    = 32;
    private static final int MIN_HEIGHT   = 32;

    private static final int LEVEL1_WIDTH = 64;
    private static final int LEVEL2_WIDTH = 128;
    private static final int LEVEL3_WIDTH = 256;
    private static final int LEVEL4_WIDTH = 512;

    static void setSimple()
    {
        boolean isAnimating = MainWindow.ROOT.layoutWindow.isAnimating();

        int numChildren = MainWindow.ROOT.desk.getNumChildren();

        for (int i = 0; i < numChildren; i++) {
            Widget widget = MainWindow.ROOT.desk.getChild(i);

            if (widget instanceof CardUI) {
                CardUI cardUI = (CardUI) widget;

                boolean isSimple = isAnimating || (cardUI.getWidth() < LEVEL2_WIDTH);

                if (isSimple != cardUI.isSimple) {
                    cardUI.htmlPane.setVisible(!isSimple);
                    cardUI.lblFloatingTitle.setVisible(isSimple);

                    cardUI.setTheme(isSimple ? "cardui" : "cardui-title");
                    cardUI.reapplyTheme();

                    cardUI.isSimple = isSimple;
                }
            }
        }
    }

    boolean isSimple = false;

    private boolean hasCloseButton = false;

    private final Runnable closeCallback = new Runnable() {
        @Override
        public void run()
        {
            removeCard(href);
        }
    };

    @Override
    protected void layout()
    {
        boolean isAnimating = MainWindow.ROOT.layoutWindow.isAnimating();

        int width0  = getWidth();
        int height0 = getHeight();

        int width  = Math.max(width0,  MIN_WIDTH );
        int height = Math.max(height0, MIN_HEIGHT);

        if (width0 < MIN_WIDTH || height0 < MIN_HEIGHT) {
            setSize(width, height);
        }

        if (!isAnimating) {
            htmlPane.setSize(getInnerWidth(), getInnerHeight());
            htmlPane.setPosition(getInnerX(), getInnerY());
        }

        if (isSimple) {
            lblFloatingTitle.setSize(lblFloatingTitle.getPreferredWidth(), lblFloatingTitle.getPreferredHeight());

            int cardCenterX = getInnerX() + getInnerWidth ()/2;
            int cardCenterY = getInnerY() + getInnerHeight()/2;

            int centerX = cardCenterX - lblFloatingTitle.getWidth ()/2;
            int centerY = cardCenterY - lblFloatingTitle.getHeight()/2;

            lblFloatingTitle.setPosition(centerX, centerY);
        }

        setupCloseButton();

        layoutTitle();

        layoutCloseButton();
    }

    private void setupCloseButton()
    {
        if (getWidth() < LEVEL2_WIDTH) {
            if (hasCloseButton) {
                removeCloseCallback(closeCallback);
                hasCloseButton = false;
            }
        }
        else {
            if (!hasCloseButton) {
                addCloseCallback(closeCallback);
                hasCloseButton = true;
            }
        }
    }

    public ModelPaper getModelPaper()
    {
        return modelPaper;
    }
}
