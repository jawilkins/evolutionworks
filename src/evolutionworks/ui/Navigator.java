package evolutionworks.ui;

public interface Navigator
{
    void navigateTo(String href, String referer, boolean addCloseCommand);
    void close(String href);
    void addToReadingList(String href, boolean addCloseCommand);
    void removeFromReadingList(String href, boolean addCloseCommand);
    void showReadingList(boolean showCloseCommand);
}
