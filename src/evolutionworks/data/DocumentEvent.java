package evolutionworks.data;

import java.util.EventObject;

import ecologylab.semantics.metadata.builtins.Document;

/**
 * @author gaurav_kl
 *
 */
public class DocumentEvent extends EventObject
{
    /**
     * auto-generated UID because this class is Serializable
     */
    private static final long serialVersionUID = -1824516344967403102L;

    /**
     * @param document
     */
    DocumentEvent(Document document)
    {
        super(document);
    }

    /* (non-Javadoc)
     * @see java.util.EventObject#getSource()
     */
    @Override
    public Document getSource()
    {
        return (Document)(super.getSource());
    }
}
