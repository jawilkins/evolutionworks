package evolutionworks.data;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;

import ecologylab.net.ParsedURL;
import ecologylab.semantics.collecting.SemanticsSessionScope;
import ecologylab.semantics.compiler.orm.MetadataORMFacade;
import ecologylab.semantics.cyberneko.CybernekoWrapper;
import ecologylab.semantics.generated.library.RepositoryMetadataTranslationScope;
import ecologylab.semantics.generated.library.search.GoogleScholarSearch;
import ecologylab.semantics.metadata.builtins.Document;
import ecologylab.semantics.metadata.builtins.DocumentClosure;
import ecologylab.semantics.metametadata.MetaMetadataRepository;
import ecologylab.serialization.SimplTypesScope;
import ecologylab.serialization.TranslationContext;
import ecologylab.serialization.TranslationContextPool;

/**
 * @author gaurav_kl
 * 
 */
public final class DocumentQuery
{
    private static boolean                 isInitialized = false;

    // MetametaData
    private static TranslationContext      translationContext;
    private static SimplTypesScope         metadataTScope;
    private static SemanticsSessionScope   semanticsScope;

    // ORM cache
    private static boolean                 cacheEnabled;
    private static DocumentCacheInitThread cacheInitThread;

    private static Logger                  LOGGER        = Logger.getLogger(DocumentQuery.class.getName());

    /**
     * @param cacheEnabled
     */
    public static void initialize(boolean cacheEnabled)
    {
        if (!isInitialized) {
            translationContext = TranslationContextPool.get().acquire();

            SimplTypesScope.enableGraphSerialization();

            // this has to be before RepositoryMetadataTranslationScope.get()
            MetaMetadataRepository.initializeTypes();

            metadataTScope = RepositoryMetadataTranslationScope.get();

            semanticsScope = new SemanticsSessionScope(metadataTScope, CybernekoWrapper.class);

            enableCache(cacheEnabled);

            isInitialized = true;
        }

        assertInitialized();
    }

    /**
     * 
     */
    static void assertInitialized()
    {
        assert translationContext != null;
        assert metadataTScope != null;
        assert semanticsScope != null;

        if (cacheEnabled) {
            assert cacheInitThread != null;
        }
    }

    /**
     */
    public static void shutdown()
    {
        enableCache(false);

        semanticsScope.getDownloadMonitors().stop(false);
    }

    private static final long DEFAULT_QUERY_TIMEOUT = 30000;                // 30 seconds

    private static long       queryTimeout          = DEFAULT_QUERY_TIMEOUT;

    /**
     * @param parsedURL
     * @return
     */
    public static Document query(ParsedURL parsedURL)
    {
        assertInitialized();

        DocumentContinuation documentContinuation = new DocumentContinuation();

        try {
            synchronized (documentContinuation) {
                documentContinuation.retrieveDocument(parsedURL, null);

                long startTime = System.currentTimeMillis();

                while (!documentContinuation.hasDocument()) {
                    boolean timedOut =
                        queryTimeout != 0 && (System.currentTimeMillis() - startTime) >= queryTimeout;

                    if (!timedOut) {
                        documentContinuation.wait(queryTimeout);
                    }
                    else {
                        LOGGER.warning(DocumentQuery.class.getName() + " Query Unreponsive (Timeout = " +
                            Long.toString(queryTimeout) + "ms): URL = " + parsedURL);

                        documentContinuation.stop();

                        return null;
                    }
                }
            }
        }
        catch (InterruptedException e) {
            LOGGER.log(Level.WARNING, DocumentQuery.class.getName() + " Query Interrupted: URL = " + parsedURL, e);

            return null;
        }
        catch (RuntimeException e) {
            LOGGER.log(Level.WARNING, DocumentQuery.class.getName() + " Query Exception: URL = " + parsedURL, e);

            return null;
        }

        return documentContinuation.getDocument();
    }

    /**
     * @param keywords - an array of keywords
     * @return
     */
    public static GoogleScholarSearch query(String[] keywords)
    {
        ParsedURL parsedURL = makeKeywordURL(keywords);

        Document document = query(parsedURL);

        if (document instanceof GoogleScholarSearch) {
            return (GoogleScholarSearch) document;
        }
        else {
            throw new IllegalArgumentException("A keyword query should result in a GoogleScholarSearch.");
        }
    }

    /**
     * @param parsedURL
     * @param listener
     */
    public static void queryAsync(ParsedURL parsedURL, DocumentEventListener listener)
    {
        assertInitialized();

        DocumentContinuation documentContinuation = new DocumentContinuation();

        try {
            documentContinuation.retrieveDocument(parsedURL, listener);
        }
        catch (Exception e) {
            LOGGER.log(Level.WARNING, "Asynchronous Query failed: URL = " + parsedURL, e);

            // let listener know that there is no document
            listener.recieveDocument(null);
        }
    }

    /**
     * @param keywords - an array of keywords
     * @param listener - an event listener to receive the document
     */
    public static void queryAsync(String keywords[], DocumentEventListener listener)
    {
        ParsedURL parsedURL = makeKeywordURL(keywords);

        queryAsync(parsedURL, listener);
    }

    /**
     * @param keywords
     * @return
     */
    private static ParsedURL makeKeywordURL(String[] keywords)
    {
        if (keywords.length > 0) {
            String plussedKeywords = plusKeywords(keywords);
            String url = "http://scholar.google.com/scholar?q=" + plussedKeywords + "+site:dl.acm.org";

            ParsedURL parsedURL = ParsedURL.getAbsolute(url, "Query URL");

            return parsedURL;
        }
        else {
            throw new IllegalArgumentException("no keywords given");
        }
    }

    /**
     * @param keywords
     * @return
     */
    private static String plusKeywords(String[] keywords)
    {
        StringBuilder builder = new StringBuilder();

        if (keywords.length > 0) {
            builder.append(keywords[0]);

            for (int i = 1; i < keywords.length; i++) {
                builder.append('+');
                builder.append(keywords[i]);
            }
        }

        String plussedKeywords = builder.toString();

        return plussedKeywords;
    }

    /**
     * @param parsedUrl
     * @return
     */
    static List<Document> retrieveCachedDocument(ParsedURL parsedUrl)
    {
        if (cacheEnabled && cacheInitThread.isCompleted()) {
            final MetadataORMFacade ormFacade = cacheInitThread.getOrmFacade();
            final Session hibernateSession = cacheInitThread.getHibernateSession();
            
            synchronized (ormFacade) {
                return ormFacade.lookupDocumentByLocation(hibernateSession, parsedUrl);
            }
        }
        else {
            return null;
        }
    }

    /**
     * @param cachedDocument
     */
    static boolean materializeDocument(Document cachedDocument)
    {
        assert cacheEnabled;

        try {
            if (cacheInitThread.isCompleted()) {
                final MetadataORMFacade ormFacade = cacheInitThread.getOrmFacade();
            
                synchronized (ormFacade) {
                    ormFacade.materialize(cachedDocument, translationContext, null);
                }

                return true;
            }
            else {
                return false;
            }
        }
        catch (InterruptedException e) {
            LOGGER.log(Level.WARNING, "Materialize Interrupted", e);

            return false;
        }
        catch (RuntimeException e) {
            LOGGER.log(Level.WARNING, "Materialize Exception", e);

            return false;
        }
    }

    /**
     * @param documentContinuation
     * @param parsedURL
     */
    static Document retrieveDocumentAsync(DocumentContinuation documentContinuation, ParsedURL parsedURL)
    {
        Document queueDocument = semanticsScope.getOrConstructDocument(parsedURL);
        queueDocument.queueDownload(documentContinuation);

        return queueDocument;
    }

    /**
     * @param incomingClosure
     * @return
     * @throws InterruptedException 
     */
    static Document retrieveAndSaveDocument(DocumentClosure incomingClosure)
    {
        incomingClosure.getDocument().setDownloadStatus(1);
        final Document newDocument = incomingClosure.getDocument();

        if (cacheEnabled && cacheInitThread.isCompleted()) {
            final MetadataORMFacade ormFacade = cacheInitThread.getOrmFacade();
            final Session hibernateSession = cacheInitThread.getHibernateSession();

            materializeDocument(newDocument);

            new Thread(new Runnable() {

                @Override
                public void run()
                {
                    synchronized (ormFacade) {
                        ormFacade.recursivelySave(hibernateSession, newDocument);
                    }
                }

            }).start();
        }

        return newDocument;
    }

    /**
     * @return whether the cache is enabled
     */
    public static boolean isCacheEnabled()
    {
        return cacheEnabled;
    }

    /**
     * @param cacheEnabled - whether caching should be enabled
     */
    public static void enableCache(boolean cacheEnabled)
    {
        DocumentQuery.cacheEnabled = cacheEnabled;

        if (cacheEnabled) {
            if (cacheInitThread == null) {
                cacheInitThread = new DocumentCacheInitThread();
                cacheInitThread.start();
            }
            // else
            //    just keep using the current one if enabling multiple times 
        }
        else {
            if (cacheInitThread != null) {
                cacheInitThread.disable();
                cacheInitThread = null;
            }            
            // else
            //    already disabled 
        }
    }

    /**
      * private constructor because utility classes should not have instances
      */
    private DocumentQuery()
    {}

    /**
     * @return the queryTimeout (milliseconds)
     */
    public static long getQueryTimeout()
    {
        return queryTimeout;
    }

    /**
     * @param queryTimeout the queryTimeout to set (milliseconds)
     * 
     * Set to 0 to wait indefinitely.
     * 
     */
    public static void setQueryTimeout(long queryTimeout)
    {
        DocumentQuery.queryTimeout = queryTimeout;
    }

}
