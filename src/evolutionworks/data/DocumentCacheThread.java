package evolutionworks.data;

import java.util.List;

import ecologylab.net.ParsedURL;
import ecologylab.semantics.generated.library.creativeWork.CreativeWork;
import ecologylab.semantics.generated.library.search.GoogleScholarSearch;
import ecologylab.semantics.metadata.builtins.Document;

public class DocumentCacheThread extends Thread
{
    private Document document = null;
    final private ParsedURL parsedURL;

    public DocumentCacheThread(ParsedURL parsedURL)
    {
        this.parsedURL = parsedURL;
    }
    
    public boolean hasDocument()
    {
        return document != null;
    }
    
    public Document getDocument()
    {
        return document;
    }
    
    @Override
    public void run()
    {
        List<Document> documentList = DocumentQuery.retrieveCachedDocument(parsedURL);

        if (documentList != null) {
            for (int i = 0; i < documentList.size(); i++) {
                Document cachedDocument = documentList.get(i);

                if (cachedDocument instanceof GoogleScholarSearch ||
                    (cachedDocument instanceof CreativeWork && isFullyPopulated(cachedDocument)) &&
                    (DocumentQuery.materializeDocument(cachedDocument))) {

                    document = cachedDocument;
                }
            }
        }
    }

    private boolean isFullyPopulated(Document document)
    {
        return document.getDownloadStatus() != 0;
    }
    
}
