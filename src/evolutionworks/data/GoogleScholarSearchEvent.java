package evolutionworks.data;

import java.util.EventObject;

import ecologylab.semantics.generated.library.search.GoogleScholarSearch;

/**
 * @author gaurav_kl
 *
 */
public class GoogleScholarSearchEvent extends EventObject
{
    /**
     * auto-generated UID because this class is Serializable
     */
    private static final long serialVersionUID = 3283695581245056095L;

    /**
     * @param googleScholarSearch
     */
    GoogleScholarSearchEvent(GoogleScholarSearch googleScholarSearch)
    {
        super(googleScholarSearch);
    }

    /* (non-Javadoc)
     * @see java.util.EventObject#getSource()
     */
    @Override
    public GoogleScholarSearch getSource()
    {
        return (GoogleScholarSearch)(super.getSource());
    }
}
