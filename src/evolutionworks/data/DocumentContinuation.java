package evolutionworks.data;

import java.util.logging.Logger;

import ecologylab.generic.Continuation;
import ecologylab.io.DownloadProcessor;
import ecologylab.net.ParsedURL;
import ecologylab.semantics.metadata.builtins.Document;
import ecologylab.semantics.metadata.builtins.DocumentClosure;

class DocumentContinuation implements Continuation<DocumentClosure>
{
    private static Logger         LOGGER = Logger.getLogger(DocumentContinuation.class.getName());

    private DocumentEventListener listener;

    public synchronized void callback(DocumentClosure incomingClosure)
    {
        document = DocumentQuery.retrieveAndSaveDocument(incomingClosure);

        if (!triggerEvent()) {
            // no listener so wake up the other thread so they can check for the document
            // notifyAll is used to make sure the right thread gets woke up
            notifyAll();
        }
    }

    private boolean triggerEvent()
    {
        if (listener != null) {
            // there is a listener, so we were called asynchronously, so send an event
            DocumentEvent documentEvent = new DocumentEvent(document);
            listener.recieveDocument(documentEvent);

            return true;
        }
        else {
            return false;
        }
    }

    private Document document;

    private Document queueDocument;

    synchronized void retrieveDocument(ParsedURL parsedURL, DocumentEventListener listener)
    {
        this.listener = listener;

        if (!retrieveCachedDocument(parsedURL)) {
            queueDocument = DocumentQuery.retrieveDocumentAsync(this, parsedURL);
        }
    }

    private static long DEFAULT_CACHE_TIMEOUT = 30000;
    private static long cacheTimeout          = DEFAULT_CACHE_TIMEOUT;

    private synchronized boolean retrieveCachedDocument(ParsedURL parsedURL)
    {
        if (!DocumentQuery.isCacheEnabled()) {
            return false;
        }
        
        DocumentCacheThread documentCacheThread = new DocumentCacheThread(parsedURL);
        documentCacheThread.start();

        try {
            long startTime = System.currentTimeMillis();

            documentCacheThread.join(cacheTimeout);
                    
            boolean timedOut = cacheTimeout != 0 && (System.currentTimeMillis() - startTime) >= cacheTimeout;

            if (!documentCacheThread.hasDocument()) {
                if (timedOut) {
                    LOGGER.warning(DocumentContinuation.class.getName() + " Cache Unresponsive (Timeout = " + Long.toString(cacheTimeout) + "ms): URL = " + parsedURL);
                }
                
                documentCacheThread.interrupt();
                return false;
            }
        }
        catch (InterruptedException e) {
            LOGGER.warning(DocumentContinuation.class.getName() + " Cache Interrupted: URL = " + parsedURL);

            return false;
        }
        catch (RuntimeException e) {
            LOGGER.warning(DocumentContinuation.class.getName() + " Cache Exception: URL = " + parsedURL);

            return false;
        }

        document = documentCacheThread.getDocument();

        return document != null;
    }

    public void stop()
    {
        if (queueDocument != null) {
            DocumentClosure documentClosure = queueDocument.getOrConstructClosure();

            if (documentClosure != null) {
                DownloadProcessor<DocumentClosure> downloadMonitor = documentClosure.downloadMonitor();

                if (downloadMonitor != null) {
                    downloadMonitor.requestStop();
                }
            }
        }
    }

    private boolean isVerbose = false;

    /**
     * @return if the document will be serialized to the console after it is retrieved
     */
    boolean isVerbose()
    {
        return isVerbose;
    }

    /**
     * @param isVerbose set if the object will be serialized to the console after it is retrieved
     */
    void setVerbose(boolean isVerbose)
    {
        this.isVerbose = isVerbose;
    }

    /**
     * @return if document has been retrieved yet
     */
    synchronized boolean hasDocument()
    {
        return document != null;
    }

    /**
     * @return the document
     */
    synchronized Document getDocument()
    {
        return document;
    }

    /**
     * @return the cacheTimeout
     */
    public static long getCacheTimeout()
    {
        return cacheTimeout;
    }

    /**
     * @param cacheTimeout the cacheTimeout to set
     */
    public static void setCacheTimeout(long cacheTimeout)
    {
        DocumentContinuation.cacheTimeout = cacheTimeout;
    }

}
