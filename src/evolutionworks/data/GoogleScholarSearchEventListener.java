package evolutionworks.data;

/**
 * @author gaurav_kl
 *
 */
public interface GoogleScholarSearchEventListener
{
    /**
     * @param googleScholarSearchEvent
     */
    void recieveDocument(GoogleScholarSearchEvent googleScholarSearchEvent);
}
