package evolutionworks.data;

import org.hibernate.Session;

import ecologylab.semantics.compiler.orm.MetadataORMFacade;

public class DocumentCacheInitThread extends Thread
{
    private boolean isCompleted = false;
    private MetadataORMFacade ormFacade;
    private Session hibernateSession;
    
    @Override
    public
    void run()
    {
        ormFacade = MetadataORMFacade.defaultSingleton();
        hibernateSession = ormFacade.newSession();
        isCompleted = true;
    }

    /**
     * @return the isCompleted
     */
    public boolean isCompleted()
    {
        return isCompleted;
    }

    /**
     * @return the hibernateSession
     */
    public Session getHibernateSession()
    {
        return hibernateSession;
    }

    /**
     * @return the ormFacade
     */
    public MetadataORMFacade getOrmFacade()
    {
        return ormFacade;
    }

    public void disable()
    {
        if (hibernateSession != null) {
            hibernateSession.close();
        }

        hibernateSession = null;
        ormFacade = null;
        
        isCompleted = false;
    }

}
