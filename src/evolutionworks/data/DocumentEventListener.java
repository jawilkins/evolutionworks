package evolutionworks.data;

/**
 * @author gaurav_kl
 *
 */
public interface DocumentEventListener
{
    /**
     * @param documentEvent
     */
    void recieveDocument(DocumentEvent documentEvent);
    
}
