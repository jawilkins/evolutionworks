package evolutionworks;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import au.com.bytecode.opencsv.CSVReader;
import evolutionworks.model.ModelPaper;
import evolutionworks.model.ModelQuery;

public class Stats
{
    static BufferedWriter myOut;
    
    static void log(String msg)
    {
        try {
            myOut.write(msg);
            myOut.write("\n");
            myOut.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args)
    {
        evolutionworks.model.ModelQuery.initialize(true);
        evolutionworks.model.ModelQuery.setQueryTimeout(120000);
        
        try{
        Thread.sleep(60000);
        }catch(Exception e){
            e.printStackTrace();
        }
        try {
            myOut = new BufferedWriter(new FileWriter("citationCounts.txt", false));
        }
        catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
                
        try {
            final CSVReader csvReader = new CSVReader(new BufferedReader(new FileReader("log4.csv")));
            final List<String[]> csv = csvReader.readAll();

            String[] excludeUsers = {};// "jwilkins", "gaurav", "ajit" };
            String[] excludeKeywords = {};//s "human centered computing" };
            int[] includeSessions = {
                    39, 45, 50, 52, 53, 55, 56, 63, 66, 72, 74, 
                    3, 4,
                    78,79,82,83,87,86 };
            
            Filter[] filters = {
                    new Filter() {
                        boolean test(String[] line)
                        {
                            return
                            line[IS_BROWSER].trim().equals("false") &&
                                line[TYPE].trim().equals("timer") &&
                                line[SUBTYPE].trim().equals("total") &&
                                line[KEYWORDS].trim().equals("human centered computing wiimote");
                        }

                    },

                    new Filter() {
                        boolean test(String[] line)
                        {
                            return
                            line[IS_BROWSER].trim().equals("false") &&
                                line[TYPE].trim().equals("timer") &&
                                line[SUBTYPE].trim().equals("total") &&
                                (line[KEYWORDS].trim().equals("human centered computing health care") ||
                                line[KEYWORDS].trim().equals("human centered computing image health care"));
                        }

                    },

                    new Filter() {
                        boolean test(String[] line)
                        {
                            return
                            line[IS_BROWSER].trim().equals("true") &&
                                line[TYPE].trim().equals("timer") &&
                                line[SUBTYPE].trim().equals("total") &&
                                line[KEYWORDS].trim().equals("human centered computing wiimote");
                        }

                    },

                    new Filter() {
                        boolean test(String[] line)
                        {
                            return
                            line[IS_BROWSER].trim().equals("true") &&
                                line[TYPE].trim().equals("timer") &&
                                line[SUBTYPE].trim().equals("total") &&
                                (line[KEYWORDS].trim().equals("human centered computing health care") ||
                                line[KEYWORDS].trim().equals("human centered computing image health care"));
                        }

                    },

                    new Filter() {
                        Collection<String> papers = new HashSet<String>();

                        boolean test(String[] line)
                        {
                            boolean include =
                                line[IS_BROWSER].trim().equals("false") &&
                                    line[TYPE].trim().equals("reading-list") &&
                                    !line[SUBTYPE].trim().equals("size") &&
                                    line[KEYWORDS].trim().equals("human centered computing wiimote");
                            
                            if (include) {
                                return papers.add(line[DATA].trim());
                                
                            }
                            else {
                                return false;
                            }
                        }
                    },

                    new Filter() {
                        Collection<String> papers = new HashSet<String>();

                        boolean test(String[] line)
                        {
                            boolean include =
                                line[IS_BROWSER].trim().equals("false") &&
                                    line[TYPE].trim().equals("reading-list") &&
                                    !line[SUBTYPE].trim().equals("size") &&
                                    (line[KEYWORDS].trim().equals("human centered computing health care") ||
                                    line[KEYWORDS].trim().equals("human centered computing image health care"));
                            
                            if (include) {
                                return papers.add(line[DATA].trim());
                                
                            }
                            else {
                                return false;
                            }
                        }
                    },
                    
                    new Filter() {
                        Collection<String> papers = new HashSet<String>();

                        boolean test(String[] line)
                        {
                            boolean include =
                                line[IS_BROWSER].trim().equals("true") &&
                                    line[TYPE].trim().equals("reading-list") &&
                                    !line[SUBTYPE].trim().equals("size") &&
                                    line[KEYWORDS].trim().equals("human centered computing wiimote");
                            
                            if (include) {
                                return papers.add(line[DATA].trim());
                                
                            }
                            else {
                                return false;
                            }
                        }
                    },
                    
                    new Filter() {
                        Collection<String> papers = new LinkedHashSet<String>();

                        boolean test(String[] line)
                        {
                            boolean include =
                                line[IS_BROWSER].trim().equals("true") &&
                                    line[TYPE].trim().equals("reading-list") &&
                                    !line[SUBTYPE].trim().equals("size") &&
                                    (line[KEYWORDS].trim().equals("human centered computing health care") ||
                                    line[KEYWORDS].trim().equals("human centered computing image health care"));
                            
                            if (include) {
                                return papers.add(line[DATA].trim());
                                
                            }
                            else {
                                return false;
                            }
                        }
                    },

                    new Filter() {
                        Collection<String> papers = new LinkedHashSet<String>();

                        boolean test(String[] line)
                        {
                            boolean include =
                                    line[TYPE].trim().equals("timer") &&
                                    line[SUBTYPE].trim().equals("total") &&
                                    (line[KEYWORDS].trim().equals("human centered computing health care") ||
                                     line[KEYWORDS].trim().equals("human centered computing image health care") ||
                                     line[KEYWORDS].trim().equals("human centered computing wiimote"));
                            
                            if (include) {
                                return papers.add(line[SESSION].trim());
                                
                            }
                            else {
                                return false;
                            }
                        }
                    },

                    new Filter() {
                        Collection<String> papers = new HashSet<String>();

                        boolean test(String[] line)
                        {
                            boolean include =
                                    line[TYPE].trim().equals("reading-list") &&
                                    !line[SUBTYPE].trim().equals("size") &&
                                    line[KEYWORDS].trim().equals("human centered computing wiimote");
                            
                            if (include) {
                                return papers.add(line[DATA].trim());
                                
                            }
                            else {
                                return false;
                            }
                        }
                    },

                    new Filter() {
                        Collection<String> papers = new HashSet<String>();

                        boolean test(String[] line)
                        {
                            boolean include =
                                    line[TYPE].trim().equals("reading-list") &&
                                    !line[SUBTYPE].trim().equals("size") &&
                                    (line[KEYWORDS].trim().equals("human centered computing health care") ||
                                    line[KEYWORDS].trim().equals("human centered computing image health care"));
                            
                            if (include) {
                                return papers.add(line[DATA].trim());
                                
                            }
                            else {
                                return false;
                            }
                        }
                    },
            };

            printTotals(csv, excludeUsers, excludeKeywords, includeSessions, filters);

            log("--graph/wiimote--");
            filters[0].print();
            filters[0].printStats();
            filters[4].print();
            filters[4].printCitationTotal();
            log("--graph/healthcare--");
            filters[1].print();
            filters[1].printStats();
            filters[5].print();
            filters[5].printCitationTotal();
            log("--browser/wiimote--");
            filters[2].print();
            filters[2].printStats();
            filters[6].print();
            filters[6].printCitationTotal();
            log("--browser/healthcare--");
            filters[3].print();
            filters[3].printStats();
            filters[7].print();
            filters[7].printCitationTotal();
            log("--total--");
            filters[8].print();
            log("--wiimote--");
            filters[9].print();
            filters[9].printCitationTotal();
            log("--healthcare--");
            filters[10].print();
            filters[10].printCitationTotal();
            
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private final static int SESSION    = 0;
    private final static int TIME       = 1;
    private final static int MACHINE    = 2;
    private final static int USER       = 3;
    private final static int IS_BROWSER = 4;
    private final static int IS_TEST    = 5;
    private final static int KEYWORDS   = 6;
    private final static int LIST_SIZE  = 7;
    private final static int STARTTIME  = 8;
    private final static int TIMESTAMP  = 9;
    private final static int TYPE       = 10;
    private final static int SUBTYPE    = 11;
    private final static int DATA       = 12;

    static abstract class Filter
    {
        abstract boolean test(String[] line);

        void print()
        {
            for (String[] line : results) {
                log(StringUtils.join(line, ","));
            }
            log(Integer.toString(results.size()));
        }

        void printCitationTotal()
        {
            int citationTotal = 0;
            
            for (String[] line : results) {
                ModelPaper modelPaper = ModelQuery.query(line[DATA].trim());
                
                if (modelPaper != null) {
                    log("(" + modelPaper.getCitationCount() + ") " + line[DATA].trim());
                    citationTotal += modelPaper.getCitationCount();
                }
            }

            log("citation total: " + citationTotal);
        }
        
        String toTime(double ms)
        {
            return Integer.toString((int) (ms / 60000.0)) + ":" + Integer.toString((int) (ms / 1000.0) % 60) + ":" +
                Integer.toString((int) (ms) % 1000);
        }

        void printStats()
        {
            int max = -1, min = -1, total = 0, count = 0;
            double avg = -1;
            boolean isFirst = true;

            for (String[] line : results) {
                int val = Integer.parseInt(line[DATA].trim());

                if (isFirst) {
                    max = val;
                    min = val;
                    isFirst = false;
                }
                else {
                    if (val > max) {
                        max = val;
                    }

                    if (val < min) {
                        min = val;
                    }
                }

                total += val;

                count++;
            }

            if (count > 0) {
                avg = (double) total / (double) count;

                log("Stats: ");
                log("min:" + toTime((double) min));
                log("max:" + toTime((double) max));
                log("tot:" + toTime((double) total));
                log("cnt:" + count);
                log("avg:" + toTime(avg));
            }
        }

        Collection<String[]> results = new LinkedList<String[]>();
    }

    public static void printTotals(
        List<String[]> csv,
        String[] excludeUsers,
        String[] excludeKeywords,
        int[] includeSessions,
        Filter filters[])
    {
        for (String[] line : csv) {
            //log(line[TYPE] + line[SUBTYPE]);
            boolean excluded = false;
            for (String excludeUser : excludeUsers) {
                if (excludeUser.equals(line[USER].trim())) {
                    excluded = true;
                    break;
                }
            }

            for (String excludeKeyword : excludeKeywords) {
                if (excludeKeyword.equals(line[KEYWORDS].trim())) {
                    excluded = true;
                    break;
                }
            }

            {
                boolean found = false;
                for (int includeSession : includeSessions) {
                    if (includeSession == Integer.parseInt(line[SESSION].trim())) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    excluded = true;
                }
            }

            if (!excluded) {
                for (Filter filter : filters) {
                    if (filter.test(line)) {
                        filter.results.add(line);
                    }
                }
            }
        }
    }
}
