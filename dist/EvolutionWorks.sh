#!/bin/bash

export CLASSPATH=./bin/EvolutionWorks.jar
export CLASSPATH=${CLASSPATH}:./lib/crossPlatformSemantics/cyberneko
export CLASSPATH=${CLASSPATH}:./lib/crossPlatformSemantics/ecologylabFundamental
export CLASSPATH=${CLASSPATH}:./lib/crossPlatformSemantics/ecologylabSemantics
export CLASSPATH=${CLASSPATH}:./lib/crossPlatformSemantics/ecologylabSemanticsCyberneko
export CLASSPATH=${CLASSPATH}:./lib/crossPlatformSemantics/simplTranslators
export CLASSPATH=${CLASSPATH}:./lib/crossPlatformSemantics/ecologylabGeneratedSemantics
export CLASSPATH=${CLASSPATH}:./lib/crossPlatformSemantics/MetaMetadataRepository
export CLASSPATH=${CLASSPATH}:./lib/ecologylabSemanticsORM
export CLASSPATH=${CLASSPATH}:./lib/hibernate3.jar
export CLASSPATH=${CLASSPATH}:./lib/hibernate-jpa-2.0-api-1.0.1.Final.jar
export CLASSPATH=${CLASSPATH}:./lib/c3p0-0.9.1.2.jar
export CLASSPATH=${CLASSPATH}:./lib/commons-collections-3.1.jar
export CLASSPATH=${CLASSPATH}:./lib/dom4j-1.6.1.jar
export CLASSPATH=${CLASSPATH}:./lib/ehcache-core-2.4.6.jar
export CLASSPATH=${CLASSPATH}:./lib/gephi-toolkit.jar
export CLASSPATH=${CLASSPATH}:./lib/javassist-3.14.0-GA.jar
export CLASSPATH=${CLASSPATH}:./lib/jta-1.1.jar
export CLASSPATH=${CLASSPATH}:./lib/loremipsum-1.0.jar
export CLASSPATH=${CLASSPATH}:./lib/lwjgl.jar
export CLASSPATH=${CLASSPATH}:./lib/opencsv-2.3.jar
export CLASSPATH=${CLASSPATH}:./lib/postgresql-9.0-801.jdbc4.jar
export CLASSPATH=${CLASSPATH}:./lib/slf4j-api-1.6.4.jar
export CLASSPATH=${CLASSPATH}:./lib/slf4j-jdk14-1.6.4.jar
export CLASSPATH=${CLASSPATH}:./lib/TWL.jar
export CLASSPATH=${CLASSPATH}:./lib/TWLEffects.jar
export CLASSPATH=${CLASSPATH}:./lib/xpp3-1.1.4c.jar
export CLASSPATH=${CLASSPATH}:./lib/commons-lang3-3.3.1.jar
export CLASSPATH=${CLASSPATH}:./lib/xerces-2.4.0.jar

if [ "$(uname)" == "Darwin" ];
then
   LWJGL_SYSTEM=macosx
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ];
then
   LWJGL_SYSTEM=linux
elif [ -n "$COMSPEC" -a -x "$COMSPEC" ];
then
   LWJGL_SYSTEM=windows
fi

java -Xverify:none -ea -Xms1024m -Xmx1024m -Djava.library.path=./lib/native/${LWJGL_SYSTEM} evolutionworks.Browser jwilkins Nakoruru graph "gpgpu" 5
