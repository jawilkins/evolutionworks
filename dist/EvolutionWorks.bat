@echo off

set CLASSPATH=.\bin\EvolutionWorks.jar
set CLASSPATH=%CLASSPATH%;.\lib\crossPlatformSemantics\cyberneko
set CLASSPATH=%CLASSPATH%;.\lib\crossPlatformSemantics\ecologylabFundamental
set CLASSPATH=%CLASSPATH%;.\lib\crossPlatformSemantics\ecologylabSemantics
set CLASSPATH=%CLASSPATH%;.\lib\crossPlatformSemantics\ecologylabSemanticsCyberneko
set CLASSPATH=%CLASSPATH%;.\lib\crossPlatformSemantics\simplTranslators
set CLASSPATH=%CLASSPATH%;.\lib\crossPlatformSemantics\ecologylabGeneratedSemantics
set CLASSPATH=%CLASSPATH%;.\lib\crossPlatformSemantics\MetaMetadataRepository
set CLASSPATH=%CLASSPATH%;.\lib\ecologylabSemanticsORM
set CLASSPATH=%CLASSPATH%;.\lib\hibernate3.jar
set CLASSPATH=%CLASSPATH%;.\lib\hibernate-jpa-2.0-api-1.0.1.Final.jar
set CLASSPATH=%CLASSPATH%;.\lib\c3p0-0.9.1.2.jar
set CLASSPATH=%CLASSPATH%;.\lib\commons-collections-3.1.jar
set CLASSPATH=%CLASSPATH%;.\lib\commons-lang3-3.3.1.jar
set CLASSPATH=%CLASSPATH%;.\lib\dom4j-1.6.1.jar
set CLASSPATH=%CLASSPATH%;.\lib\ehcache-core-2.4.6.jar
set CLASSPATH=%CLASSPATH%;.\lib\gephi-toolkit.jar
set CLASSPATH=%CLASSPATH%;.\lib\javassist-3.14.0-GA.jar
set CLASSPATH=%CLASSPATH%;.\lib\jta-1.1.jar
set CLASSPATH=%CLASSPATH%;.\lib\loremipsum-1.0.jar
set CLASSPATH=%CLASSPATH%;.\lib\lwjgl.jar
set CLASSPATH=%CLASSPATH%;.\lib\opencsv-2.3.jar
set CLASSPATH=%CLASSPATH%;.\lib\postgresql-9.0-801.jdbc4.jar
set CLASSPATH=%CLASSPATH%;.\lib\slf4j-api-1.6.4.jar
set CLASSPATH=%CLASSPATH%;.\lib\slf4j-jdk14-1.6.4.jar
set CLASSPATH=%CLASSPATH%;.\lib\TWL.jar
set CLASSPATH=%CLASSPATH%;.\lib\TWLEffects.jar
set CLASSPATH=%CLASSPATH%;.\lib\xpp3-1.1.4c.jar
set CLASSPATH=%CLASSPATH%;.\lib\xerces-2.4.0.jar

java -Xverify:none -ea -Xms1024m -Xmx1024m -Djava.library.path=.\lib\native\windows evolutionworks.Browser jwilkins Nakoruru graph "gpgpu" 5

pause