import fileinput
import sys
import re
import os

prog1  = re.compile("^(?P<file>.*):(?P<line>.*):(?P<category>.*):(?P<text>.*)$")
prog2  = re.compile("^Note:(?P<text>.*)$")
prog3  = re.compile("^(?P<category>.*):(?P<text>.*)$")

for line in fileinput.input():
    m1  = prog1.match(line)
    m2  = prog2.match(line)
    m3  = prog3.match(line)

    if m1:
        sys.stdout.write(os.getcwd() + os.path.sep + m1.group('file') + '(' + m1.group('line') + '):' + m1.group('category') + ':' + m1.group('text') + "\n")
    elif m2:
        sys.stdout.write('LINK : warning :' + m2.group('text') + "\n")
    elif m3:
        sys.stdout.write('LINK : ' + m3.group('category') + ' :' + m3.group('text') + "\n")
    else:
        sys.stdout.write(line)
